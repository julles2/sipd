<?php
/**
 * Velin Admin Panel - Backend For Laravel 5
 *
 * This Auto Functions file
 * 
 * @author   Muhamad Reza Abdul Rohim <reza.wikrama3@gmail.com>
 * 
 */

function velin()
{
	return new \Velin\Skeletons\Site();
}

function urlBackend($slug)
{
	return velin()->urlBackend($slug);
}

function redirectBackendAction($action)
{
	return velin()->redirectBackendAction($action);
}

function urlBackendAction($action)
{
	return velin()->urlBackendAction($action);
}

function injectModel($model)
{
	$masterModel = 'App\\Models\\'.$model;

	return new $masterModel();
}

function user()
{
	return \Auth::user();
}

function roman($integer, $upcase = true) 
{ 
    $table = array('M'=>1000, 'CM'=>900, 'D'=>500, 'CD'=>400, 'C'=>100, 'XC'=>90, 'L'=>50, 'XL'=>40, 'X'=>10, 'IX'=>9, 'V'=>5, 'IV'=>4, 'I'=>1); 
    $return = ''; 
    while($integer > 0) 
    { 
        foreach($table as $rom=>$arb) 
        { 
            if($integer >= $arb) 
            { 
                $integer -= $arb; 
                $return .= $rom; 
                break; 
            } 
        } 
    } 

    return $return; 
}

function findHeader()
{
  $model = injectModel('Header')->find(1);
  
  return $model;  
}

function breadcrumbs($lists = [])
{
    $count = count($lists);

    $str = "";

    $no = 0;

    foreach($lists as $title => $url)
    {
        $no++;

        $str .= "<a href = '$url'>$title</a>";

        if($no != $count)
        {
            $str .= ' » ';
        }
    }

    return $str;
} 

function kata_titik($word)
{
    $maks = 90;
    $strlenword = strlen($word);
    $jmlTitik = $maks - $strlenword;
    $titik = "";
    for($a=0;$a<$jmlTitik;$a++)
    {
        $titik.=".";
    }

    return $word.$titik.$a;
}

function romawi($integer, $upcase = true) 
{ 
    $table = array('M'=>1000, 'CM'=>900, 'D'=>500, 'CD'=>400, 'C'=>100, 'XC'=>90, 'L'=>50, 'XL'=>40, 'X'=>10, 'IX'=>9, 'V'=>5, 'IV'=>4, 'I'=>1); 
    $return = ''; 
    while($integer > 0) 
    { 
        foreach($table as $rom=>$arb) 
        { 
            if($integer >= $arb) 
            { 
                $integer -= $arb; 
                $return .= $rom; 
                break; 
            } 
        } 
    } 

    return $return; 
} 


    function TableOfContents($depth)
        /*AutoTOC function written by Alex Freeman
        * Released under CC-by-sa 3.0 license
        * http://www.10stripe.com/  */
        {
        $filename = public_path('contents/tes.pdf');
        //read in the file
        $file = fopen($filename,"r");
        $html_string = fread($file, filesize($filename));
        fclose($file);
     
        //get the headings down to the specified depth
        $pattern = '/<h[2-'.$depth.']*[^>]*>.*?<\/h[2-'.$depth.']>/';
        $whocares = preg_match_all($pattern,$html_string,$winners);
     
        //reformat the results to be more usable
        $heads = implode("\n",$winners[0]);
        $heads = str_replace('<a name="','<a href="#',$heads);
        $heads = str_replace('</a>','',$heads);
        $heads = preg_replace('/<h([1-'.$depth.'])>/','<li class="toc$1">',$heads);
        $heads = preg_replace('/<\/h[1-'.$depth.']>/','</a></li>',$heads);
     
        //plug the results into appropriate HTML tags
        $contents = '<div id="toc"> 
        <p id="toc-header">Contents</p>
        <ul>
        '.$heads.'
        </ul>
        </div>';
        echo $contents;
    }


    function one_line($output)
    {
        $output = str_replace(array("\r\n", "\r"), "\n", $output);
        $lines = explode("\n", $output);
        $new_lines = array();

        foreach ($lines as $i => $line) {
            if(!empty($line))
                $new_lines[] = trim($line);
        }
        echo implode($new_lines);
    }