<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\FotoDetail;

class Foto extends Model
{
    public $guarded = [];

    public function details()
    {
    	return $this->hasMany(FotoDetail::class,'foto_id');
    }
}
