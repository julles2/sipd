<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\User;
use App\Models\Element;

class Nilai extends Model
{
    public $guarded = [];

    public function user()
    {
    	return $this->belongsTo(User::class , 'user_id');
    }


    public function element()
    {
    	return $this->belongsTo(Element::class , 'element_id');
    }
}	
