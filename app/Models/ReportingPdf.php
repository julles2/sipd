<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\User;
use App\Models\ReportingPdfKelompokData;

class ReportingPdf extends Model
{
    public $guarded = [];

    public $table = 'reporting_pdf';

   	public function user()
   	{
   		return $this->belongsTo(User::class,'user_id');
   	}

   	public function pdfKelompoks()
   	{
   		return $this->hasMany(ReportingPdfKelompokData::class,'pdf_id');
   	}
}
