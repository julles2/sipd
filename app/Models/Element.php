<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

use App\Models\JenisData;
use App\Models\Satuan;
use App\Models\User;
use App\Models\Nilai;

class Element extends Model
{
    public $guarded = ['kelompok_data_id'];

    public function jenisData()
    {
    	return $this->belongsTo(JenisData::class);
    }

    public function satuan()
    {
    	return $this->belongsTo(Satuan::class);
    }

    public function parent()
    {
    	return $this->belongsTo(Element::class,'parent_id');
    }

    public function childs()
    {
    	return $this->hasMany(Element::class,'parent_id');
    }

    public function users()
    {
        return $this->belongsToMany(User::class,'nilais')
        ->pivot('id');
    }

    public function nilais()
    {
        return $this->hasMany(Nilai::class,'element_id');
    }
}
