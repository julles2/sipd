<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\Element;

class Satuan extends Model
{
    public $guarded = [];

    public function elements()
    {
    	return $this->hasMany(Element::class,'satuan_d');
    }
}
