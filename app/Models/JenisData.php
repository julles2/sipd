<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\KelompokData;
use App\Models\Element;

class JenisData extends Model
{
   public $guarded = [];

   public function kelompokData()
   {
   		return $this->belongsTo(KelompokData::class,'kelompok_data_id');
   }

   public function elements()
    {
    	return $this->hasMany(Element::class,'jenis_data_id');
    }
}
