<?php namespace App\Models\Repositories;

class ElementRepositories
{
    public function __construct()
    {
        $this->model = injectModel('Element');
    }

    public function data($kelompokDataId)
    {
        $kelompok_data_id = request()->get('kelompok_data_id');
    
        $jenis_data_id = request()->get('jenis_data_id');
    
        $element = request()->get('element');
        
        $model = $this->model;

        $jenisData = injectModel('JenisData');

        if(!empty($kelompok_data_id))
        {
            $arrayJenisDataId = $jenisData
                ->select('id')
                ->whereKelompokDataId($kelompok_data_id)
                ->get()
                ->toArray();
            
            if(!empty($jenis_data_id))
            {
                $model = $model->whereJenisDataId($jenis_data_id);
            }else{
                $model = $model->whereIn('jenis_data_id', $arrayJenisDataId);
            }
        }else{
            
            $arrayJenisDataId = $jenisData
                ->select('id')
                ->whereKelompokDataId($kelompokDataId)
                ->get()
                ->toArray();

            $model = $model->whereIn('jenis_data_id', $arrayJenisDataId);
        }

        return $model->whereParentId(null)->paginate(1);
    }

    public function numberOrHuruf($no)
    {
        if($no % 2 != 0) 
        {
            $range = range('a','z');
        }else{
            $range = range(1,1000);
        }

        return $range;
    }

    public function cariAnakStatus($child)
    {
        $count = $child->childs->count();

        if($count > 0)
        {
            return $child->status;
        }else{
            return '';
        }
    }

    public function childs($parents,$no)
    {
        $str = "";
        
        $padding = 20 * $no;

        $huruf = $this->numberOrHuruf($no);

        $counter = 0;

        foreach($parents->childs as $child)
        {   
            $noChild = $no + 1;
            $str .= "<tr>";
            
            $str .= "<td style = 'padding-left:".$padding."px;'>";
                $str .= $huruf[$counter].' . '.$child->element.' '.$this->cariAnakStatus($child);
            $str .= "</td>";
            
            $str .= "<td>".$child->satuan->satuan."</td>";

            $str .= "<td>".\Velin::buttons($child->id)."</td>";

            $str .= "</tr>";
            
            $str .= $this->childs($child,$noChild);
            
            $counter++;
        }
        
        return $str;
    }

    public function hitungNilaiAnak($child,$year="",$userId="")
    {
        $model = $child->childs()->first();

        if(!empty($model->id))
        {

            if(!empty($userId))
            {
                $hasil = 0;

                foreach($child->childs as $row)
                {
                    $getNilai = $row->nilais()
                    ->where('year',$year)
                    ->where('user_id',$userId)
                    ->first();
                    
                    $searchNilai = !(empty($getNilai->id)) ? $getNilai->nilai : 0;

                    $hasil = $hasil + $searchNilai;
                }

            }else{
                $hasil = 0;

                foreach($child->childs as $row)
                {
                    $getNilai = $row->nilais()
                    ->select('id',\DB::raw('SUM(nilai) as nilai'))
                    ->where('year',$year)
                    ->first();
                    
                    $searchNilai = !(empty($getNilai->id)) ? $getNilai->nilai : 0;

                    $hasil = $hasil + $searchNilai;
                }
            }

        }else{
            $hasil = '';
        }

        return $hasil;
    }

    public function hitungNilaiRataAnak($child,$year="",$userId="")
    {
        $model = $child->childs()->first();

        if(!empty($model->id))
        {

            $hasil = 0;
            $no = 0;
            foreach($child->childs as $row)
            {
                $no++;
                $getNilai = $row->nilais()
                ->select('id',\DB::raw('SUM(nilai) as nilai'))
                ->where('year',$year);

                if(!empty($userId))
                {
                    $getNilai = $getNilai->where('user_id',$userId);
                }
                
                $getNilai = $getNilai->first();
                
                $searchNilai = !(empty($getNilai->id)) ? $getNilai->nilai : 0;

                $hasil = $hasil + $searchNilai;
            }
            if($no>0)
            {
                $hasil = $hasil / $no; 
            }
        }else{
            $hasil = '';
        }

        return $hasil;
    }

    public function valueNilai($child,$year="",$userId="")
    {
        $model = $child->nilais()
        ->select('id', \DB::raw('SUM(nilai) as nilai'),'sumber_data')
        ->where('year',$year);

        if(!empty($userId))
        {
         $model = $model->whereUserId($userId);
        }
        
        $model = $model->first();

        if(!empty($model->id))
        {
            return $model;
        }else{
            return '';
        }
    }

    public function textOrFormula($child ,$year = "" , $userId = "")
    {
        $count = $child->childs()->count();

        if($count < 1)
        {
            $nilaiId = "nilai$child->id";

            $sumberDataId = "sumberDataId$child->id";

            $valueNilai = $this->valueNilai($child , $year , $userId);

            (!empty($userId)) ? $nilai = "<input type = 'text' value = '".@$valueNilai->nilai."' id = '$nilaiId' onkeyup = \"insertNilai('$nilaiId','$sumberDataId', ".$child->id.")\" />" : $nilai = @$valueNilai->nilai;

            (!empty($userId)) ? $sumber_data = "<input type = 'text' value = '".@$valueNilai->sumber_data."' id = '$sumberDataId' onkeyup = \"insertNilai('$nilaiId','$sumberDataId', ".$child->id.")\" />" : $sumber_data = '';

        }else{
            if($child->status == '**')
            {
                $nilai = $this->hitungNilaiAnak($child,$year,$userId);
            }elseif($child->status == '***'){
             $nilai = $this->hitungNilaiRataAnak($child,$year,$userId);
            }else{
                $nilai = 'n/a';
            }

             $sumber_data = '';
        }

        return [
            'nilai' => $nilai,
            'sumber_data'   => $sumber_data,
        ];
    }

    public function childsWithNilai($parents,$no , $year = "" , $userId = "")
    {
        // dd($parents);
        $str = "";
        
        $padding = 20 * $no;

        $huruf = $this->numberOrHuruf($no);

        $counter = 0;

        foreach($parents->childs as $child)
        {   
            $noChild = $no + 1;
            
            $textOrFormula = $this->textOrFormula($child,$year,$userId);

            $str .= "<tr>";
            
            $str .= "<td style = 'padding-left:".$padding."px;'>";
                @$str .= @$huruf[$counter].' . '.@$child->element.' '.@$this->cariAnakStatus($child);
            $str .= "</td>";
            
            $str .= "<td>".$textOrFormula['nilai']."</td>";

            $str .= "<td>".$textOrFormula['sumber_data']."</td>";

            $str .= "<td>".$child->satuan->satuan."</td>";

            $str .= "<td><a class='iframe' data-fancybox-type='iframe' href='".urlBackendAction('chart?id='.$child->id.'&skpd_id='.request()->get('skpd_id'))."' id = 'click'>Chart</a></td>";

            $str .= "</tr>";
            
            $str .= $this->childsWithNilai($child,$noChild , $year,$userId);
            
            $counter++;
        }
        
        return $str;
    }

    public function cariAnak($id)
    {
        $model = injectModel('Element')->select('id')->where('parent_id',$id)->get();

        $id = "";

        foreach($model as $row)
        {
            $id.= $row->id.',';

            $id.= $this->cariAnak($row->id);
        }

        return $id;
    }

    public function cariBapak($id)
    {
        $model = injectModel('Element')->find($id);

        $count = $model->parent()->count();
       
        $hasil = '';

        if($count > 0)
        {
            
            $parent = $model->parent()->first();

            if(!empty($parent->id))
            {
                $hasil .= $parent->id;

                $hasil .= ','.$this->cariBapak($parent->id);
            }

        }else{
            $hasil = '';
        }

       return $hasil;
    }
}