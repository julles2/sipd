<?php namespace App\Models\Repositories;

use DB;

class NilaiRepositories
{
    public function __construct()
    {
        $this->model = injectModel('Element');
    }

    public function parents($jenis_data_id = "")
    {
        if (!empty($jenis_data_id)) {
            $jenis_data_id = $jenis_data_id;
        } else {
            $jenis_data_id = request()->get('jenis_data_id');
        }

        return $this->model->where('parent_id', null)
            ->where('jenis_data_id', $jenis_data_id)->get();
    }

    public function cariAnakStatus($child)
    {
        $count = $child->childs->count();

        if ($count > 0) {
            return $child->status;
        } else {
            return '';
        }
    }

    public function numberOrHuruf($no)
    {
        if ($no % 2 != 0) {
            $range = range('a', 'z');
        } else {
            $range = range(1, 1000);
        }

        return $range;
    }

    public function valueNilai($child, $year = "", $userId = "")
    {
        $model = $child->nilais()
            ->select('id', \DB::raw('SUM(nilai) as nilai'), 'sumber_data')
            ->where('year', $year);

        if (!empty($userId)) {
            $model = $model->whereUserId($userId);
        }

        $model = $model->first();

        if (!empty($model->id)) {
            return $model;
        } else {
            return '';
        }
    }

    public function valueNilaiPdf($child, $year = "", $endYear = "", $userId = "")
    {
        $endYear = !empty($endYear) ? $endYear : $year;

        $model = $child->nilais()
            ->select('id', \DB::raw('SUM(nilai) as nilai'), 'sumber_data')
            ->whereBetween("year", [$year, $endYear]);

        if (!empty($userId)) {
            $model = $model->whereUserId($userId);
        }

        $model = $model->first();

        if (!empty($model->id)) {
            return $model;
        } else {
            return '';
        }
    }

    public function hitungNilaiAnak($child, $year = "", $userId = "")
    {
        $model = $child->childs()->first();

        if (!empty($model->id)) {

            if (!empty($userId)) {
                $hasil = 0;

                foreach ($child->childs as $row) {
                    $getNilai = $row->nilais()
                        ->where('year', $year)
                        ->where('user_id', $userId)
                        ->first();

                    $searchNilai = !(empty($getNilai->id)) ? $getNilai->nilai : 0;

                    $hasil = $hasil + $searchNilai;
                }

            } else {
                $hasil = 0;

                foreach ($child->childs as $row) {
                    $getNilai = $row->nilais()
                        ->select('id', \DB::raw('SUM(nilai) as nilai'))
                        ->where('year', $year)
                        ->first();

                    $searchNilai = !(empty($getNilai->id)) ? $getNilai->nilai : 0;

                    $hasil = $hasil + $searchNilai;
                }
            }

        } else {
            $hasil = '';
        }

        return $hasil;
    }

    public function hitungNilaiAnakPdf($child, $year = "", $endYear = "", $userId = "")
    {
        $model = $child->childs()->first();
        $endYear = !empty($endYear) ? $endYear : $year;
        if (!empty($model->id)) {

            if (!empty($userId)) {
                $hasil = 0;

                foreach ($child->childs as $row) {
                    $getNilai = $row->nilais()
                        ->select(DB::raw("SUM(nilai) as nilai,id"))
                        ->whereBetween("year", [$year, $endYear])
                        ->where('user_id', $userId)
                        ->first();

                    $searchNilai = !(empty($getNilai->id)) ? $getNilai->nilai : 0;

                    $hasil = $hasil + $searchNilai;
                }

            } else {
                $hasil = 0;

                foreach ($child->childs as $row) {
                    $getNilai = $row->nilais()
                        ->select('id', \DB::raw('SUM(nilai) as nilai'))
                        ->whereBetween("year", [$year, $endYear])
                        ->first();

                    $searchNilai = !(empty($getNilai->id)) ? $getNilai->nilai : 0;

                    $hasil = $hasil + $searchNilai;
                }
            }

        } else {
            $hasil = '';
        }

        return $hasil;
    }

    public function hitungNilaiRataAnak($child, $year = "", $userId = "")
    {
        $model = $child->childs()->first();

        if (!empty($model->id)) {

            $hasil = 0;
            $no = 0;
            foreach ($child->childs as $row) {
                $no++;
                $getNilai = $row->nilais()
                    ->select('id', \DB::raw('SUM(nilai) as nilai'))
                    ->where('year', $year);

                if (!empty($userId)) {
                    $getNilai = $getNilai->where('user_id', $userId);
                }

                $getNilai = $getNilai->first();

                $searchNilai = !(empty($getNilai->id)) ? $getNilai->nilai : 0;

                $hasil = $hasil + $searchNilai;
            }
            if ($no > 0) {
                $hasil = $hasil / $no;
            }
        } else {
            $hasil = '';
        }

        return $hasil;
    }

    public function hitungNilaiRataAnakPdf($child, $year = "", $endYear, $userId = "")
    {
        $model = $child->childs()->first();
        $endYear = !empty($endYear) ? $endYear : $year;
        if (!empty($model->id)) {

            $hasil = 0;
            $no = 0;
            foreach ($child->childs as $row) {
                $no++;
                $getNilai = $row->nilais()
                    ->select('id', \DB::raw('SUM(nilai) as nilai'))
                    ->whereBetween("year", [$year, $endYear]);

                if (!empty($userId)) {
                    $getNilai = $getNilai->where('user_id', $userId);
                }

                $getNilai = $getNilai->first();

                $searchNilai = !(empty($getNilai->id)) ? $getNilai->nilai : 0;

                $hasil = $hasil + $searchNilai;
            }
            if ($no > 0) {
                $hasil = $hasil / $no;
            }
        } else {
            $hasil = '';
        }

        return $hasil;
    }

    public function nilaiOrFormula($child, $year = "", $userId = "")
    {
        $count = $child->childs()->count();
        // dd($child);
        if ($count < 1) {
            $nilaiId = "nilai$child->id";

            $sumberDataId = "sumberDataId$child->id";

            $valueNilai = $this->valueNilai($child, $year, $userId);

            $nilai = @$valueNilai->nilai;

            $sumber_data = @$valueNilai->sumber_data;
        } else {
            if ($child->status == '**') {
                $nilai = $this->hitungNilaiAnak($child, $year, $userId);

            } elseif ($child->status == '***') {

                $nilai = $this->hitungNilaiRataAnak($child, $year, $userId);

            } else {
                $nilai = 'n/a';
            }

            $sumber_data = '';
        }

        return [
            'nilai' => $nilai,
            'sumber_data' => $sumber_data,
        ];
    }

    public function nilaiOrFormulaPdf($child, $k = "", $endYear, $userId = "")
    {
        $count = $child->childs()->count();

        if ($count < 1) {
            $nilaiId = "nilai$child->id";

            $sumberDataId = "sumberDataId$child->id";

            $valueNilai = $this->valueNilaiPdf($child, $year, $endYear, $userId);

            $nilai = @$valueNilai->nilai;

            $sumber_data = @$valueNilai->sumber_data;
        } else {
            if ($child->status == '**') {
                $nilai = $this->hitungNilaiAnakPdf($child, $year, $endYear, $userId);

            } elseif ($child->status == '***') {

                $nilai = $this->hitungNilaiRataAnakPdf($child, $year, $endYear, $userId);

            } else {
                $nilai = 'n/a';
            }

            $sumber_data = '';
        }

        return [
            'nilai' => $nilai,
            'sumber_data' => $sumber_data,
        ];
    }

    public function childs($parents, $no, $year = "", $userId = "")
    {
        $str = "";

        $padding = 20 * $no;

        $huruf = $this->numberOrHuruf($no);

        $counter = 0;

        foreach ($parents->childs as $child) {
            $noChild = $no + 1;

            $nilaiOrFormula = $this->nilaiOrFormula($child, $year, $userId);

            $str .= "<tr>";

            $str .= "<td style = 'padding-left:" . $padding . "px;'>";
            $str .= $huruf[$counter] . ' . ' . $child->element . ' ' . $this->cariAnakStatus($child);
            $str .= "</td>";

            $str .= "<td>" . $nilaiOrFormula['nilai'] . "</td>";

            $str .= "<td>" . $child->satuan->satuan . "</td>";

            $str .= "<td>" . $nilaiOrFormula['sumber_data'] . "</td>";

            $str .= "<td style = 'text-align:center;'><a href='" . $this->generateUrlChart($child->id) . "'><i class='fa fa-align-justify'></i> </a></td>";

            $str .= "</tr>";

            $str .= $this->childs($child, $noChild, $year, $userId);

            $counter++;
        }

        return $str;
    }

    public function childPdf($parents, $no, $k = "", $userId = "")
    {
        $str = "";

        $padding = 21 * $no;

        $huruf = $this->numberOrHuruf($no);

        $counter = 0;

        foreach ($parents->childs as $child) {
            $noChild = $no + 1;

            $str .= "<tr>";

            $str .= "<td style = 'padding-left:" . $padding . "px;'>";
            $str .= $huruf[$counter] . ' . ' . $child->element . ' ' . $this->cariAnakStatus($child);
            $str .= "</td>";

            for ($a = $k->year; $a <= $k->end_year2; $a++) {

                $nilaiOrFormula = $this->nilaiOrFormula($child, $a, $userId);

                $str .= "<td>" . $nilaiOrFormula['nilai'] . "</td>";
            }

            $str .= "<td>" . $child->satuan->satuan . "</td>";

            // $str .= "<td>" . $nilaiOrFormula['sumber_data'] . "</td>";
            // $str .= "<td>-</td>";
            $str .= "</tr>";

            $str .= $this->childPdf($child, $noChild, $k, $userId);

            $counter++;
        }

        return $str;
    }

    public function childPdf2($parents, $no, $year = "", $userId = "")
    {
        $str = "";

        $padding = 21 * $no;

        $huruf = $this->numberOrHuruf($no);

        $counter = 0;

        foreach ($parents->childs as $child) {
            $noChild = $no + 1;

            $nilaiOrFormula = $this->nilaiOrFormulaPdf($child, $year, $userId);

            $str .= "<tr>";

            $str .= "<td style = 'padding-left:" . $padding . "px;'>";
            $str .= $huruf[$counter] . ' . ' . $child->element . ' ' . $this->cariAnakStatus($child);
            $str .= "</td>";

            $str .= "<td>" . $nilaiOrFormula['nilai'] . "</td>";

            $str .= "<td>" . $child->satuan->satuan . "</td>";

            $str .= "<td>" . $nilaiOrFormula['sumber_data'] . "</td>";

            $str .= "</tr>";

            $str .= $this->childPdf($child, $noChild, $year, $userId);

            $counter++;
        }

        return $str;
    }

    public function generateUrlChart($element_id)
    {
        $oldUrl = request()->fullUrl();

        $exp = explode("?", $oldUrl);

        $newUrl = url('nilai/chart?element_id=' . $element_id . '&' . $exp[1]);

        return $newUrl;
    }

    public function childArray($parents, $no, $year = "", $userId = "")
    {
        $str = "";

        $padding = 20 * $no;

        $huruf = $this->numberOrHuruf($no);

        $counter = 0;

        $result = [];

        foreach ($parents->childs as $child) {
            $noChild = $no + 1;
            $nilaiOrFormula = $this->nilaiOrFormula($child, $year, $userId);
            $result[] = [
                $huruf[$counter] . ' . ' . $child->element . ' ' . $this->cariAnakStatus($child),
                $nilaiOrFormula['nilai'],
                $child->satuan->satuan,
                $nilaiOrFormula['sumber_data'],

            ];

            $result[] = $this->childArray($child, $noChild, $year, $userId);

            $counter++;
        }

        return $result;
    }

    public function childExcel($parents, $no, $year = "", $userId = "")
    {
        $str = "";

        $padding = 20 * $no;

        $huruf = $this->numberOrHuruf($no);

        $counter = 0;

        $awal = date('Y') - 6;

        foreach ($parents->childs as $child) {
            $noChild = $no + 1;

            $nilaiOrFormula = $this->nilaiOrFormula($child, $year, $userId);

            $str .= "<tr>";

            $str .= "<td style = 'padding-left:" . $padding . "px;'>";
            $str .= $huruf[$counter] . ' . ' . $child->element . ' ' . $this->cariAnakStatus($child);
            $str .= "</td>";

            for ($a = $awal; $a <= date('Y'); $a++) {
                $nilaiAtauFormula = $this->nilaiOrFormula($child, $a, $userId);
                $str .= "<td>$nilaiAtauFormula[nilai]</td>";
            }

            $str .= "<td>" . $child->satuan->satuan . "</td>";

            $str .= "<td>" . $nilaiOrFormula['sumber_data'] . "</td>";

            $str .= "<td style = 'text-align:center;'><a href='" . $this->generateUrlChart($child->id) . "'><i class='fa fa-align-justify'></i> </a></td>";

            $str .= "</tr>";

            $str .= $this->childExcel($child, $noChild, $year, $userId);

            $counter++;
        }

        return $str;
    }
}
