<?php

namespace App\Models;

use App\Models\KelompokData;
use App\Models\ReportingPdfJenisData;
use Illuminate\Database\Eloquent\Model;

class ReportingPdfKelompokData extends Model
{
    public $guarded = [];

    public function kelompok()
    {
        return $this->belongsTo(KelompokData::class, 'kelompok_data_id');
    }

    public function pdfJenisDatas()
    {
        return $this->hasMany(ReportingPdfJenisData::class, 'kelompok_pdf_id');
    }

    public function getEndYear2Attribute()
    {
        $result = $this->end_year;

        if (empty($result)) {
            $result = $this->year;
        }

        return $result;
    }
}
