<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

use App\Models\ReportingPdfKelompokData;
use App\Models\JenisData;

class ReportingPdfJenisData extends Model
{
	public $table = 'reporting_pdf_jenis_data';

    public $guarded = [];

    public function pdfJenisData()
    {
    	return $this->belongsTo(ReportingPdfKelompokData::class,'kelompok_pdf_id');
    }

    public function jenisData()
    {
    	return $this->belongsTo(JenisData::class,'jenis_data_id');
    }
}
