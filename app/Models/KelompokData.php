<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\KelompokData;

class KelompokData extends Model
{
    public $guarded = [];

    public function jenisDatas()
    {
    	return $this->hasMany(JenisData::class,'kelompok_data_id');
    }
}
