<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Models\Intro;

class DashboardController extends Controller
{
	public function __construct(Intro $intro)
	{
		$this->intro = $intro;
	}

    public function getIndex()
    {
    	$intro = $this->intro->find(1);

    	return view('dashboard',compact('intro'));
    }
}
