<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

use App\Models\Link;

class LinkController extends Controller
{
    public function getIndex()
    {
    	$breadcrumbs = [
    		'Link'=> url('link'),
    		'Listing'=>'#',
    	];

    	$links = Link::all();

    	return view('link',compact('links','breadcrumbs'));
    }
}
