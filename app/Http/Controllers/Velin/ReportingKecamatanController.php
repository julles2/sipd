<?php

namespace App\Http\Controllers\Velin;

use App\Http\Controllers\Velin\VelinController;
use App\Models\Element;
use App\Models\JenisData;
use App\Models\KelompokData;
use App\Models\Repositories\NilaiRepositories;
use App\Models\User;
use Illuminate\Http\Request;

class ReportingKecamatanController extends VelinController
{

    public function __construct()
    {
        parent::__construct();
        $this->view = 'velin.reporting.kecamatan.';
    }

    public function kecamatan($input = "")
    {
        $query = User::whereType('skpd');

        $query = $query->pluck('name', 'id')
            ->toArray();

        return ['all' => 'All Kecamatan'] + $query;
    }

    public function kelompokData()
    {
        $query = KelompokData::orderBy('id', 'asc')->pluck('kelompok_data', 'id')->toArray();

        return $query;
    }

    public function jenisData($kelompokDataId = null)
    {
        if (empty($kelompokDataId)) {
            $kelompokDataId = KelompokData::orderBy('id', 'asc')->first()->id;
        }

        $query = JenisData::whereKelompokDataId($kelompokDataId)->pluck('jenis_data', 'id')->toArray();
        return $query;
    }

    public function getJenisData($kelompokDataId)
    {
        $query = $this->jenisData($kelompokDataId);

        return response()->json([
            'results' => $query,
        ]);
    }

    public function element($jenisDataId = null)
    {

        if (empty($jenisDataId)) {
            $kelompokData = KelompokData::with('jenisDatas')->orderBy('id', 'asc')->first();
            $jenisDataId  = $kelompokData->jenisDatas()->first()->id;
        }

        $result  = [];
        $parents = Element::with('childs')->whereParentId(null)->where('jenis_data_id', $jenisDataId)->get();

        foreach ($parents as $parent) {
            $result[$parent->id] = $parent->element;

            foreach ($parent->childs()->with('childs')->get() as $child) {
                $result[$child->id] = "-- " . $child->element;

                foreach ($child->childs()->with('childs')->get() as $gc) {
                    $result[$gc->id] = "---- " . $gc->element;
                }
            }
        }

        return $result;
    }

    public function getElement($jenisDataId)
    {
        $query = $this->element($jenisDataId);

        return response()->json([
            'results' => $query,
        ]);
    }

    public function getIndex()
    {
        $kecamatan = $this->kecamatan();

        return view($this->view . 'form', [
            'kecamatan'     => $kecamatan,
            'kelompok_data' => $this->kelompokData(),
            'jenis_data'    => $this->jenisData(),
            'element'       => $this->element(),
        ]);
    }

    public function postIndex(Request $request)
    {
        $validation = \Validator::make($request->all(), ['elements' => 'required']);

        if ($validation->fails()) {
            return redirect()->back()->withInput()->withErrors($validation);
        }

        $kecamatan = User::whereType("skpd");
        if ($request['kecamatan'] != "all") {
            $kecamatan = $kecamatan->whereId($request->kecamatan);
        }

        $kecamatan = $kecamatan->get();

        $elements = Element::whereIn('id', $request->elements)->get();

        $pdf = \PDF2::loadView($this->view . 'report', [
            'kecamatan' => $kecamatan,
            'elements'  => $elements,
            'nilai'     => new NilaiRepositories(),
            'dari'      => $request->dari,
            'sampai'    => $request->sampai,
        ]);

        return $pdf->stream('kimpoy.pdf');
    }
}
