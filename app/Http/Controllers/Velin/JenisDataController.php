<?php

namespace App\Http\Controllers\Velin;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Velin\VelinController;
use Table;

use App\Models\JenisData;
use App\Repositories\Combos;

class JenisDataController extends VelinController
{
    public function __construct(JenisData $model,Combos $combo)
    {
    	parent::__construct();
    	$this->model = $model;
    	$this->combo = $combo;
    }

    public function getData()
    {
    	$model = $this->model->select('jenis_datas.id','jenis_data','kelompok_data')

    		->join('kelompok_datas','kelompok_datas.id','=','jenis_datas.kelompok_data_id');

    	$data = Table::of($model)
    		->addColumn('action' , function($model){
    			return \Velin::buttons($model->id);
    		})
    		->make(true);

        return $data;
    }

    public function getIndex()
    {
        return view('velin.master_data.jenis_data.index');
    }

    public function getCreate()
    {
        return view('velin.master_data.jenis_data._form',[
            'model' => $this->model,
            'kelompokDatas'	=> $this->combo->kelompokDatas(),
        ]);
    }

    public function postCreate(Requests\Velin\JenisDataRequest $request)
    {
        $this->model->create($request->all());

        return redirectBackendAction('index')
            ->withSuccess('Data has been saved');
    }

    public function getUpdate($id)
    {
        return view('velin.master_data.jenis_data._form',[
            'model' => $this->model->findOrFail($id),
            'kelompokDatas'	=> $this->combo->kelompokDatas(),
        ]);
    }

    public function postUpdate(Requests\Velin\JenisDataRequest $request,$id)
    {
        $this->model->findOrFail($id)->update($request->all());

        return redirectBackendAction('index')
            ->withSuccess('Data has been updated');
    }

    public function getDelete($id)
    {
        return \Velin::deleteWithCatch($this->model,$id);
    }
}
