<?php

namespace App\Http\Controllers\Velin;

use App\Http\Controllers\Velin\VelinController;
use App\Models\ReportingPdf;
use App\Models\ReportingPdfJenisData;
use App\Models\ReportingPdfKelompokData;
use App\Models\Repositories\NilaiRepositories;
use App\Repositories\Combos;
use Illuminate\Http\Request;
use Table;
use Velin;

class ReportingPdfController extends VelinController
{
    public function __construct(ReportingPdf $model, ReportingPdfKelompokData $modelData, ReportingPdfJenisData $modelPdfJenis, NilaiRepositories $repo)
    {
        parent::__construct();

        $this->view = 'velin.reporting.pdf.';

        $this->model = $model;

        $this->combo = new Combos();

        $this->modelData = $modelData;

        $this->pdfJenis = $modelPdfJenis;

        $this->repo = $repo;
    }

    public function getData()
    {
        $model = $this->model->select('reporting_pdf.id', 'cover_judul', 'users.name as name')
            ->join('users', 'users.id', '=', 'reporting_pdf.user_id');

        if (user()->type == 'skpd') {
            $model = $model->where('users.type', 'skpd')
                ->where('users.id', user()->id);
        }
        $data = Table::of($model)
            ->addColumn('action', function ($model) {
                return \Velin::buttons($model->id);
            })
            ->make(true);

        return $data;
    }

    public function getCover()
    {
        return view($this->view . 'generate.cover_tes');
    }

    public function getView($id)
    {
        return redirect(Velin::urlBackendAction('view-pdf/' . $id));
    }

    public function getIndex()
    {
        return view($this->view . 'index');
    }

    public function getCreate()
    {

        $disabled = collect([1, 2, 3, 4, 5])->toJson();

        return view($this->view . '_form', [
            'model'    => $this->model,
            'disabled' => $disabled,
        ]);
    }

    public function postCreate(Request $request)
    {
        $rules = [
            'cover_judul' => 'required|max:225|min:3',
        ];

        $this->validate($request, $rules);

        $save = $this->model->create([
            'user_id'     => $this->user->id,
            'cover_judul' => $request->cover_judul,
        ]);

        return redirect(Velin::urlBackendAction('update/' . $save->id . '?active=1'));
    }

    public function getUpdate($id)
    {
        $disabled     = collect([''])->toJson();
        $model        = $this->model->findOrFail($id);
        $pdfKelompoks = $model->pdfKelompoks;
        $years        = ['' => 'Semua Tahun'];
        for ($a = 2010; $a <= date('Y'); $a++) {
            $years[$a] = $a;
        }
        return view($this->view . '_form', [
            'model'        => $model,
            'disabled'     => $disabled,
            'pdfKelompoks' => $pdfKelompoks,
            'years'        => $years,
        ]);
    }

    public function postUpdate(Request $request, $id)
    {
        $model = $this->model->findOrFail($id);

        $rules = [
            'cover_judul'    => 'required|max:225|min:3',
            'peta_kabupaten' => 'image',
        ];

        $this->validate($request, $rules);

        $inputs = $request->all();

        $inputs['peta_kabupaten'] = $this->upload($model, 'peta_kabupaten');

        $inputs['user_id'] = $this->user->id;

        $save = $model->update($inputs);

        return redirect(Velin::urlBackendAction('update/' . $model->id));
    }

    public function getPopNilai($id)
    {
        $edit  = request()->get('edit');
        $model = $this->model->findOrFail($id);
        if (empty($edit)) {
            $modelData = $this->modelData;
        } else {
            $modelData = $this->modelData->findOrFail($edit);
        }
        $kelompokDatas = $this->combo->kelompokDatas();

        return view($this->view . 'pop.nilai', [
            'model'         => $model,
            'kelompokDatas' => $kelompokDatas,
            'modelData'     => $modelData,
        ]);
    }

    public function postPopNilai(Request $request, $id)
    {
        $edit  = request()->get('edit');
        $model = $this->model->findOrFail($id);

        $rules = [
            'kelompok_data_id' => 'required',
            'year'             => 'required',
            'end_year'         => 'required',
        ];

        $this->validate($request, $rules);

        $inputs = $request->all();

        $inputs['pdf_id'] = $model->id;
        unset($inputs['edit']);
        if (empty($edit)) {
            $modelData  = $this->modelData;
            $exec       = $modelData->create($inputs);
            $initDetail = $exec;
        } else {
            $modelData = $this->modelData->findOrFail($edit);

            // cek old data
            if ($modelData->kelompok_data_id != $inputs['kelompok_data_id']) {
                $pdfJenis = $modelData->pdfJenisDatas();

                if ($pdfJenis->count() > 0) {
                    $pdfJenis->delete();
                }
            }
            //

            $exec       = $modelData->update($inputs);
            $initDetail = $modelData;
        }

        return redirect(\Velin::urlBackendAction('pop-nilai-detail/' . $initDetail->id . '?edit=' . $edit));
    }

    public function getPopNilaiDetail($pdfKelompokId)
    {
        $modelPdfKelompokData = $this->modelData->findOrFail($pdfKelompokId);
        $jenisDatas           = $modelPdfKelompokData->kelompok->jenisDatas;
        $checked              = function ($jenisDataId) use ($modelPdfKelompokData) {
            $model = $this->pdfJenis
                ->select('id')
                ->where('jenis_data_id', $jenisDataId)
                ->where('kelompok_pdf_id', $modelPdfKelompokData->id)
                ->first();

            return !empty($model->id) ? 'checked' : 'dauy';
        };
        return view($this->view . 'pop.nilai_detail', [
            'modelPdfKelompokData' => $modelPdfKelompokData,
            'jenisDatas'           => $jenisDatas,
            'checked'              => $checked,
        ]);
    }

    public function getAjaxSave()
    {
        $pdfKelompokId = request()->get('pdf_kelompok_id');

        $modelPdfKelompokData = $this->modelData->findOrFail($pdfKelompokId);
        $this->pdfJenis->create([
            'kelompok_pdf_id' => $modelPdfKelompokData->id,
            'jenis_data_id'   => request()->get('jenis_data_id'),
        ]);
        return response()->json([
            'result' => 'ok',
        ]);
    }

    public function getAjaxDelete()
    {
        $pdfKelompokId = request()->get('pdf_kelompok_id');

        $modelPdfKelompokData = $this->modelData->findOrFail($pdfKelompokId);
        $m                    = $this->pdfJenis->where('kelompok_pdf_id', $modelPdfKelompokData->id)
            ->where('jenis_data_id', request()->get('jenis_data_id'))
            ->first();
        $m->delete();
        return response()->json([
            'result' => 'ok',
        ]);
    }

    public function getDeletePdfKelompok($id)
    {
        $model = $this->modelData->findOrFail($id);
        $pdfId = $model->pdf_id;
        // delete child

        if ($model->pdfJenisDatas()->count() > 0) {
            $model->pdfJenisDatas()->delete();
        }

        //

        $model->delete();

        return redirect(Velin::urlBackendAction('update/' . $pdfId));
    }

    public function getViewPdf($id)
    {
        ini_set('max_execution_time', 0);
        $model          = $this->model->find($id);
        $peta_kabupaten = !empty($model->peta_kabupaten) ? $model->peta_kabupaten : 'null.png';
        $petaPath       = public_path('contents/' . $peta_kabupaten);
        $peta           = file_exists($petaPath) ? $petaPath : '';

        $kelompok = $model->pdfKelompoks()->first();

        $year = !empty($kelompok->id) ? $kelompok->year : date('Y');
        $data = [
            'model' => $model,
            'peta'  => $peta,
            'repo'  => $this->repo,
            'pdf'   => new \PDF(),
            'year'  => $year,
        ];
        // return view('velin.reporting.pdf.generate', $data);
        $pdf = \PDF2::loadView('velin.reporting.pdf.generate', $data);
        return $pdf->stream('kimpoy.pdf');

    }

    public function getDelete($id)
    {
        $model = $this->model->findOrFail($id);

        $modelKelompok = $model->pdfKelompoks;

        foreach ($modelKelompok as $k) {
            if ($k->pdfJenisDatas()->count() > 0) {
                $k->pdfJenisDatas()->delete();
            }
        }

        $model->pdfKelompoks()->delete();

        $model->delete();

        return redirectBackendAction('index')
            ->withSuccess('Data has been deleted');
    }
}
