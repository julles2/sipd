<?php

namespace App\Http\Controllers\Velin;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Velin\VelinController;
use Table;

use App\Models\News;

class BeritaController extends VelinController
{
    public function __construct(News $model)
    {
    	parent::__construct();
    	$this->model = $model;
    }
    
    public function getData()
    {
    	$model = $this->model->select('id','judul','status');

    	$data = Table::of($model)
            ->addColumn('status' , function($model){
                return ($model->status == 'y') ? 'publish' : 'un publish';
            })
    		->addColumn('action' , function($model){
    			return \Velin::buttons($model->id);
    		})
    		->make(true);

        return $data;
    }

    public function getIndex()
    {
        return view('velin.konten.berita.index');
    }

    public function getCreate()
    {
        return view('velin.konten.berita._form',[
            'model' => $this->model,
        ]);
    }

    public function postCreate(Requests\Velin\BeritaRequest $request)
    {
        $this->model->create($request->all());

        return redirectBackendAction('index')
            ->withSuccess('Data has been saved');
    }

    public function getUpdate($id)
    {
        return view('velin.konten.berita._form',[
            'model' => $this->model->findOrFail($id),
        ]);
    }

    public function postUpdate(Requests\Velin\BeritaRequest $request,$id)
    {
        $this->model->findOrFail($id)->update($request->all());

        return redirectBackendAction('index')
            ->withSuccess('Data has been updated');
    }

    public function getDelete($id)
    {
        $this->model->findOrFail($id)->delete();

        return redirectBackendAction('index')
            ->withSuccess('Data has been deleted');
    }
}
