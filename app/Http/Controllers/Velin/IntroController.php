<?php

namespace App\Http\Controllers\Velin;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Velin\VelinController;

use App\Models\Intro;

class IntroController extends VelinController
{
	public function __construct(Intro $model)
	{
		parent::__construct();
		$this->model = $model;
	}

    public function getIndex()
    {
    	$model = $this->model->find(1);

    	return view('velin.konten.intro._form',compact('model'));
    }

    public function postIndex(Request $request)
    {
    	$model = $this->model->find(1);

    	$model->update($request->all());

    	return redirectBackendAction('index')
            ->withSuccess('Data has been updated');
    }
}
