<?php

namespace App\Http\Controllers\Velin;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Velin\VelinController;
use App\Models\Nilai;
use App\Models\Element;
use App\Repositories\Combos;
use App\Models\Repositories\ElementRepositories;

class NilaiController extends VelinController
{
    public function __construct(Nilai $model,Combos $combo,ElementRepositories $repo)
    {
    	parent::__construct();
    	$this->model = $model;
    	$this->combo = $combo;
    	$this->repo = $repo;
    }

    public function getIndex()
    {

    	$kelompok_data_id = request()->get('kelompok_data_id');
    	
    	$jenis_data_id = request()->get('jenis_data_id');
    	
    	$year = request()->get('year');

    	$skpd_id = request()->get('skpd_id');

    	$parents = $this->repo->data($kelompok_data_id);
		
    	$userId = $skpd_id;

		$childs = function($parent,$no) use ($year,$userId){
			return $this->repo->childsWithNilai($parent,$no,$year,$userId);
		};

		$paramsPagination = [
			'kelompok_data_id'	=> $kelompok_data_id, 
			'jenis_data_id'		=> $jenis_data_id,
			'year' 				=> $year,
			'skpd_id' 			=> $skpd_id,
		];

		$pagination = $parents->appends($paramsPagination)->links();

        $textOrFormula = function($parent) use ($year,$skpd_id){
            return $this->repo->textOrFormula($parent,$year,$skpd_id);
        };
        if(user()->type == 'skpd')
        {
            $skpd = [user()->id => user()->name];
        }else{
            $skpd = ['' => 'All'] + $this->combo->skpd();
        }

    	return view('velin.ketersediaan_data.nilai.index',[
    		'kelompokDatas'	=> $this->combo->kelompokDatas(),
    		'jenisDatas'	=> $this->combo->jenisDatas($kelompok_data_id),
    		'parents'		=> $parents,
    		'skpd'			=> $skpd,
    		'childs'		=> $childs,
    		'pagination' 	=> $pagination,
            'textOrFormula' => $textOrFormula,
    	]);
    }

    public function getJenisData()
    {
    	$kelompok_data_id = request()->get('kelompok_data_id');

    	$combo = $this->combo->jenisDatas($kelompok_data_id);
    	
    	return response()->json([
    		'comboJenisData'	=> $combo,
    	]);
    }

    public function getInsertNilai()
    {
    	$id = request()->get('id'); // id element

    	$nilai = request()->get('nilai');
    	
    	$sumber_data = request()->get('sumber_data');
    	
    	$jenis_data_id = request()->get('jenis_data_id');
    	
    	$skpd_id = request()->get('skpd_id');

    	$year = request()->get('year');
    	
    	$cek = $this->model
    		->whereUserId($skpd_id)
    		->where('element_id',$id)
    		->where('year',$year)
    		->first();

    	$data = [
    			'user_id'		=> $skpd_id,
    			'element_id'	=> $id,
    			'sumber_data'	=> $sumber_data,
    			'year'			=> $year,
    			'nilai'			=> $nilai,
    	];
       
    	if(!empty($cek))
    	{
    		$cek->update($data);
    	}else{
    		$cek = $this->model->create($data);
    	}

        // update parent
           
           $this->updateParent($cek->element_id,$data); 

        //
    }

    public function updateParent($elementId,$data)
    {
        $cariBapak = substr($this->repo->cariBapak($elementId),0,-1);
        
        $bapak =  explode(",",$cariBapak);

        $parents = injectModel('Element')->whereIn('id',$bapak)->orderBy('id','desc')->get();

        foreach($parents as $parent)
        {
            
            $cekParent = $this->model->where('user_id',$data['user_id'])
                ->where('element_id',$parent->id)
                ->where('year',$data['year'])
                ->first();


            if($parent->status == '**')
            {
                $result = $this->repo->hitungNilaiAnak($parent,$data['year'],$data['user_id']);
            }elseif($parent->status == '***'){
                $result = $this->repo->hitungNilaiRataAnak($parent,$data['year'],$data['user_id']);
            }

            // insert or update parent to nilais table
                    
                    if(!empty($cekParent->id))
                    {
                        echo 'ayaa';
                        $cekParent->update(['nilai' => $result]);
                    }else{
                        echo 'aweh';
                        $this->model->insert([
                            'user_id'       => $data['user_id'],
                            'element_id'    => $parent->id,
                            'year'          => $data['year'],
                            'nilai'         => $result,
                        ]);
                    }
                //
        }
    }

    public function getChart()
    {
        $id = request()->get('id');

        $user_id = request()->get('skpd_id');

        $model = injectModel('Element')->findOrFail($id);

        $yearRange = range(2010,date('Y'));

        $years = json_encode($yearRange);

        $datas = [];

        foreach($yearRange as $year)
        {
            $cek = $this->model
                ->select('id',\DB::raw('sum(nilai) as nilai'))
                ->where('element_id',$id)
                ->where('year',$year);
            if(!empty($user_id))
            {
                $cek = $cek->where('user_id',$user_id);
            }    
            $cek= $cek->first();

            $nilai = (!empty($cek->id)) ? $cek->nilai : 0;

            $datas[] += $nilai;
        }
        
        $datas = json_encode($datas);
        
        if(user()->role_id == 2)
        {
            $skpd = [user()->role_id => user()->name];
        }else{
            $skpd = ['' => 'All'] + $this->combo->skpd();
        }

        return view('velin.ketersediaan_data.nilai.chart',compact('model','years','datas','skpd'));
    }    
}
