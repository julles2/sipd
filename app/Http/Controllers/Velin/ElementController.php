<?php

namespace App\Http\Controllers\Velin;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Velin\VelinController;
use Table;
use App\Repositories\Combos;

use App\Models\Element;
use App\Models\Repositories\ElementRepositories;

class ElementController extends VelinController
{
    public function __construct(Element $model , Combos $combo,ElementRepositories $repo)
    {
    	parent::__construct();
    	$this->model = $model;
    	$this->combo = $combo;
    	$this->repo = $repo;
    }
    
   
    public function getIndex()
    {
    	$kelompok_data_id = injectModel('KelompokData')->select('id')->orderBy('kelompok_data','asc')->first();
        $parents = $this->repo->data(@$kelompok_data_id->id);
    	$childs = function($parent,$no){
    		return $this->repo->childs($parent,$no);
    	};
    	
    	
    	return view('velin.master_data.element.index' , [
    		'kelompokDatas'	=> $this->combo->kelompokDatas(),
    		'jenisDatas'	=> $this->combo->jenisDatas($kelompok_data_id),
        	'parents'	=> $parents,
        	'childs'	=> $childs,
            'pagination' => $parents->appends(['kelompok_data_id' => request()->get('kelompok_data_id'), 'jenis_data_id' => request()->get('jenis_data_id')])->links(),
        ]);
    }

    public function getCreate()
    {
        return view('velin.master_data.element._form',[
            'model' => $this->model,
            'kelompokDatas'	=> [''=>''] + $this->combo->kelompokDatas(),
            'jenisDatas'	=> $this->combo->jenisDatas('null'),
            'elementParents'=> $this->combo->elementParents('null'),
            'satuans'=> $this->combo->satuans(),
            'kelompokDataId'=> null,
        ]);
    }

    public function postCreate(Requests\Velin\ElementRequest $request)
    {
        $this->model->create($request->all());

        return redirectBackendAction('index')
            ->withSuccess('Data has been saved');
    }

    public function getUpdate($id)
    {
        $model = $this->model->findOrFail($id);
    	$kelompokDataId = $model->jenisData->kelompokData->id;
    	return view('velin.master_data.element._form',[
            'model' => $model,
            'kelompokDatas'	=> $this->combo->kelompokDatas(),
            'jenisDatas'	=> $this->combo->jenisDatas($kelompokDataId),
            'elementParents' => ['' => ''] + $this->combo->elementParents($model->jenis_data_id , $id),
            'satuans'	=> $this->combo->satuans(),
            'kelompokDataId'	=> $kelompokDataId,
        ]);
    }

    public function postUpdate(Requests\Velin\ElementRequest $request,$id)
    {
        $deleteNilaiElement = injectModel('Nilai')->where('element_id' , $id)->delete();

        $this->model->findOrFail($id)->update($request->all());

        return redirectBackendAction('index')
            ->withSuccess('Data has been updated');
    }

    public function getDelete($id)
    {
        return \Velin::deleteWithCatch($this->model,$id);
    }

    public function getJenisData()
    {
    	$kelompok_data_id = request()->get('kelompok_data_id');

    	$combo = $this->combo->jenisDatas($kelompok_data_id);
    	return response()->json([
    		'comboJenisData'	=> $combo,
    	]);
    }

    public function getParent()
    {
    	$jenis_data_id = request()->get('jenis_data_id');

        $id = request()->get('id');

    	$combo = $this->combo->elementParents($jenis_data_id , $id);

    	return response()->json([

    		'comboParent'  => $combo,

    	]);
    }
}
