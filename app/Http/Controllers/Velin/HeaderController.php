<?php

namespace App\Http\Controllers\Velin;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Velin\VelinController;
use App\Models\Header;
use Image;

class HeaderController extends VelinController
{
	public function __construct(Header $model)
	{
		parent::__construct();
		$this->model = $model;
	}

    public function getIndex()
    {
    	$model = $this->model->find(1);

    	return view('velin.konten.header._form' , compact('model'));
    }

    public function postIndex(Request $request)
    {
    	$model = $this->model->find(1);

    	$inputs = $request->all();

    	$inputs['logo'] = $this->handleLogo($model,$request);

    	$model->update($inputs);

    	return redirectBackendAction('index')
            ->withSuccess('Data has been updated');
    }

    public function handleLogo($model,$request)
    {
    	$logo = $request->file('logo');

    	$publicPath = public_path('contents');

    	if(!empty($logo))
    	{
    		@unlink($publicPath.'/'.$model->logo);

    		$logoName = str_random(10).'.'.$logo->getClientOriginalExtension();

    		Image::make($logo)->resize(51,60)->save(public_path('contents/'.$logoName));

    		$result = $logoName;

    	}else{
    		$result = $model->logo;
    	}

    	return $result;
    }
}
