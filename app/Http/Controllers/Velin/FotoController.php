<?php

namespace App\Http\Controllers\Velin;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Velin\VelinController;
use Table;
use Image;
use App\Models\Foto;
use App\Models\FotoDetail;

class FotoController extends VelinController
{
    public function __construct(Foto $model,FotoDetail $detail)
    {
    	parent::__construct();
    	$this->model = $model;
    	$this->detail = $detail;
    }
    
    public function getData()
    {
    	$model = $this->model->select('id','judul','status');

    	$data = Table::of($model)
            ->addColumn('status' , function($model){
                return ($model->status == 'y') ? 'publish' : 'un publish';
            })
    		->addColumn('action' , function($model){
    			return \Velin::buttons($model->id);
    		})
    		->make(true);

        return $data;
    }

    public function getIndex()
    {
        return view('velin.konten.foto.index');
    }

    public function getCreate()
    {
        return view('velin.konten.foto._form',[
            'model' => $this->model,
        ]);
    }

    public function postCreate(Requests\Velin\FotoRequest $request)
    {
    	$model = $this->model;

    	$inputs = $request->all();

    	$inputs['image'] = $this->handleImage($model,$request);

        $model->create($inputs);

        return redirectBackendAction('index')
            ->withSuccess('Data has been saved');
    }

    public function getUpdate($id)
    {
        return view('velin.konten.foto._form',[
            'model' => $this->model->findOrFail($id),
        ]);
    }

    public function postUpdate(Requests\Velin\FotoRequest $request,$id)
    {
        $model = $this->model->findOrFail($id);

        $inputs = $request->all();

        $inputs['image'] = $this->handleImage($model,$request);

        $model->update($inputs);

        return redirectBackendAction('index')
            ->withSuccess('Data has been updated');
    }

    public function getDelete($id)
    {
        $model = $this->model->findOrFail($id);

        @unlink(public_path('contents/'.$model->image));

        if($model->details()->count() > 0)
        {
        	foreach($model->details as $row)
        	{
        		@unlink(public_path('contents/'.$row->image));
        	}
        }	
        
        $model->details()->delete();

        $model->delete();

        return redirectBackendAction('index')
            ->withSuccess('Data has been deleted');
    }

    public function handleImage($model,$request)
    {
    	$image = $request->file('image');

    	$publicPath = public_path('contents');

    	if(!empty($image))
    	{
    		@unlink($publicPath.'/'.$model->image);

    		$imageName = str_random(10).'.'.$image->getClientOriginalExtension();

    		Image::make($image)->save(public_path('contents/'.$imageName));

    		$result = $imageName;

    	}else{
    		$result = $model->image;
    	}

    	return $result;
    }

    public function getView($id)
    {
    	$model = $this->model->findOrFail($id);
    	
    	$detail = $this->detail;

    	$lists = $model->details()->paginate(10);

    	return view('velin.konten.foto.detail',compact('model','detail','lists'));
    }

    public function postView(Request $request , $id)
    {
    	$model = $this->model->findOrFail($id);

    	$images = $request->file('image');
    	
    	foreach($images as $image)
    	{
    		if(!empty($image))
    		{
    			$imageName = $model->name.str_random(10).'.'.$image->getClientOriginalExtension();
    			Image::make($image)->save(public_path('contents/'.$imageName));
    			$this->detail->create([
    				'foto_id'	=> $model->id,
    				'image'		=> $imageName,
    			]);
    		}
    	}

    	return redirect()->back()->with('success','Image has been saved');
    }

    public function getDeleteDetail($id,$detailId)
    {
    	$model = $this->detail->findOrFail($detailId);
    	
    	@unlink(public_path('contents/'.$model->image));

    	$model->delete();

    	return redirect(urlBackendAction('view/'.$id))->with('success','Image has been deleted');
    

    }
}
