<?php

namespace App\Http\Controllers\Velin;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Velin\VelinController;
use Table;

use App\Models\Satuan;

class SatuanController extends VelinController
{
    public function __construct(Satuan $model)
    {
    	parent::__construct();
    	$this->model = $model;
    }
    
    public function getData()
    {
    	$model = $this->model->select('id','satuan');

    	$data = Table::of($model)
    		->addColumn('action' , function($model){
    			return \Velin::buttons($model->id);
    		})
    		->make(true);

        return $data;
    }

    public function getIndex()
    {
        return view('velin.master_data.satuan.index');
    }

    public function getCreate()
    {
        return view('velin.master_data.satuan._form',[
            'model' => $this->model,
        ]);
    }

    public function postCreate(Requests\Velin\SatuanRequest $request)
    {
        $this->model->create($request->all());

        return redirectBackendAction('index')
            ->withSuccess('Data has been saved');
    }

    public function getUpdate($id)
    {
        return view('velin.master_data.satuan._form',[
            'model' => $this->model->findOrFail($id),
        ]);
    }

    public function postUpdate(Requests\Velin\SatuanRequest $request,$id)
    {
        $this->model->findOrFail($id)->update($request->all());

        return redirectBackendAction('index')
            ->withSuccess('Data has been updated');
    }

    public function getDelete($id)
    {
        return \Velin::deleteWithCatch($this->model,$id);
    }
}
