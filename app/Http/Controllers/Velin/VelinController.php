<?php

namespace App\Http\Controllers\Velin;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Models\Menu;
use Image;

class VelinController extends Controller
{
	protected $user;

    public function __construct()
    {
    	view()->share('parentMenus' , $this->parentMenus());
    	\App::setLocale('id');
    	$this->user = auth()->user();
	}

    public function parentMenus()
    {
    	$model = new Menu;

    	return $model->whereParentId(null)->orderBy('order','asc')->get();
    }

    public function upload($model,$image)
    {
        $request = request();

        $imageInit = $request->file($image);

        $publicPath = public_path('contents');

        if(!empty($imageInit))
        {
            @unlink(public_path('contents/'.$model->{$image}));

            $imageName = str_random(10).'.'.$imageInit->getClientOriginalExtension();

            Image::make($imageInit)->save(public_path('contents/'.$imageName));

            $result = $imageName;

        }else{
            $result = @$model->{$image};
        }

        return $result;
    }
}
