<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Models\Pemda;

class PemdaController extends Controller
{

	public function __construct(Pemda $pemda)
	{
		$this->pemda = $pemda;
	}

    public function getIndex()
    {

    	$breadcrumbs = [
    		'Pemda'=> url('pemda'),
    		'Listing'=>'#',
    	];

    	$bappedas  = $this->pemda->where('type','bappeda')
    		->where('status','y')
    		->orderBy('order','asc')
    		->get();

    	$sipds  = $this->pemda->where('type','sipd')
    		->where('status','y')
    		->orderBy('order','asc')
    		->get();

    	return view('pemda',compact('breadcrumbs','bappedas','sipds'));
    }

    public function getRead($slug)
    {
    	$model = $this->pemda->where('slug',$slug)->firstOrFail();

    	$judul = $model->judul;

    	$breadcrumbs = [
    		'Pemda'=> url('pemda'),
    		ucwords($model->type) =>'#',
    		"$judul" => '#',
    	];

    	return view('pemda_detail',compact('breadcrumbs','model'));
    }
}
