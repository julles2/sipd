<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use Spatie\Browsershot\Browsershot;

class TestController extends Controller
{
    public function getIndex()
    {
        Browsershot::url('http://103.212.211.162/')
            ->setScreenshotType('pdf')
            ->save(public_path('kim.pdf'));
    }
}
