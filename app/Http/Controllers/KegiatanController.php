<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Models\Foto;
use App\Models\Video;

class KegiatanController extends Controller
{
	public function __construct(Foto $foto,Video $video)
	{
		$this->foto = $foto;
        $this->video = $video;
	}

    public function getIndex()
    {
    	$fotos = $this->foto->where('status','y')->get();

        $videos = $this->video->all();

    	$breadcrumbs = [
    		'Kegiatan'=> url('kegiatan'),
    		'Listing'=>'#',
    	];

    	return view('kegiatan',compact('fotos','breadcrumbs','videos'));
    }

    public function getDetail($slug)
    {
    	$model = $this->foto->where('slug',$slug)->firstOrFail();

    	$fotos = $model->details;

    	$breadcrumbs = [
    		'Kegiatan'=> url('kegiatan'),
    		$model->judul =>'#',
    	];

    	return view('kegiatan_detail',compact('fotos','breadcrumbs'));

    }
}
