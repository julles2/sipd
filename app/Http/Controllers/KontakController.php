<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

class KontakController extends Controller
{
    public function getIndex()
    {
    	$breadcrumbs = [
    		'Kontak'=> url('kontak'),
    	];

    	return view('kontak',compact('breadcrumbs'));
    }
}
