<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Models\KelompokData;
use App\Models\JenisData;
use App\Models\Repositories\NilaiRepositories;
use App\Repositories\Combos;
use App\Models\Nilai;
use App\User;
use Excel;

class NilaiController extends Controller
{
	public function __construct(KelompokData $kelompok,NilaiRepositories $repo,Combos $combo,Nilai $model)
	{
		$this->kelompok = $kelompok;

		$this->repo = $repo;

        $this->combo = $combo;

        $this->model = $model;
	}

    public function getIndex()
    {
    	$kelompok = $this->kelompok->all();

    	return view('nilai',compact('kelompok'));
    }

    public function getDetail()
    {
    	$getYear = request()->get('year');

    	$activeYear = function($year) use ($getYear)
    	{
    		if($year == $getYear)
    		{
    			return 'class=activetab';
    		}
    	};

    	$generateUrlYear = function($year){
    		return $this->generateUrlYear($year);
    	};

    	$childs = function($parents,$no , $year="", $userId = ""){
    		return $this->repo->childs($parents,$no , $year, $userId);
    	};

    	$nilaiOrFormula = function($parent){
            return $this->repo->nilaiOrFormula($parent,$this->get('year'),$this->get('skpd_id'));
        };

        $get = function($param){
            return $this->get($param);
        };

    	$data = [
    		'kelompok'=> $this->kelompok->all(),
    		'activeYear'=> $activeYear,
    		'generateUrlYear'=>$generateUrlYear,
    		'parents'=>$this->repo->parents(),
    		'childs'=>$childs,
    		'nilaiOrFormula'=>$nilaiOrFormula,
            'selectSkpd'=>$this->combo->skpd(),
            'get'=>$get,
            'repo'=>$this->repo,
    	];

    	return view('nilai_detail',$data);
    }

    public function generateUrlYear($year)
    {	
    	$strUrl = "nilai/detail";
    	
    	$strUrl .= "?kelompok_data_id=".$this->get('kelompok_data_id');

    	$strUrl .= "&jenis_data_id=".$this->get('jenis_data_id');
    	
    	$strUrl .= "&year=$year";
    	
    	$strUrl .="&skpd_id=";
    	
    	$url = url($strUrl);

    	return $url;
    }

    public function get($param)
    {
    	return request()->get($param);
    }

    public function getChart()
    {
        $id = $this->get('element_id');

        $user_id = $this->get('skpd_id');

        $model = injectModel('Element')->findOrFail($id);

        $yearRange = range(2010,date('Y'));

        $years = json_encode($yearRange);

        $datas = [];

        foreach($yearRange as $year)
        {
            $cek = $this->model
                ->select('id',\DB::raw('sum(nilai) as nilai'))
                ->where('element_id',$id)
                ->where('year',$year);
            if(!empty($user_id))
            {
                $cek = $cek->where('user_id',$user_id);
            }    
            $cek= $cek->first();

            $nilai = (!empty($cek->id)) ? $cek->nilai : 0;

            $datas[] += $nilai;
        }
        
        $datas = json_encode($datas);
        
        $kelompok = $this->kelompok->all();

        if(!empty($user_id))
        {
            $skpd = \App\User::findOrFail($user_id)->name;
        }else{
            $skpd = '';
        }

        $get = function($param){
            return $this->get($param);
        };

        $selectSkpd=$this->combo->skpd();

        return view('chart',compact('model','years','datas','kelompok','skpd','selectSkpd','get'));
    }

    // public function getExcel()
    // {
    //     $parents = $this->repo->parents();
    
    //     $childs = function($parents,$no , $year="", $userId = ""){
    //         return $this->repo->childArray($parents,$no , $year, $userId);
    //     };

    //     $nilaiOrFormula = function($parent){
    //         return $this->repo->nilaiOrFormula($parent,$this->get('year'),$this->get('skpd_id'));
    //     };

    //     $no=0;
    //     $result = [];
    //     foreach($parents as $parent)
    //     {
    //         $no++;   

    //         $result[] = [
    //             $no.' '.$parent->element.' '.$parent->status,
    //             $nilaiOrFormula($parent)['nilai'],
    //             $parent->satuan->satuan,
    //             $nilaiOrFormula($parent)['sumber_data'],

    //         ];  

    //         $result[] = $childs($parent,$no,$this->get('year'),$this->get('skpd_id'));
    //     }
    //     //dd($result);
    //     Excel::create('nilai',function($excel){

    //     })
    //     ->download('xls');
    // }

    public function getExcel()
    {
        $getYear = request()->get('year');

        $activeYear = function($year) use ($getYear)
        {
            if($year == $getYear)
            {
                return 'class=activetab';
            }
        };

        $generateUrlYear = function($year){
            return $this->generateUrlYear($year);
        };

        $childs = function($parents,$no , $year="", $userId = ""){
            return $this->repo->childExcel($parents,$no , $year, $userId);
        };

        $nilaiOrFormula = function($parent,$a){
            return $this->repo->nilaiOrFormula($parent,$a,$this->get('skpd_id'));
        };

        $get = function($param){
            return $this->get($param);
        };  
        $skpd = User::whereType('skpd')
            ->whereId($get('skpd_id'))
            ->first();
        
        if(!empty($skpd->id))
        {
            $nama_skpd = $skpd->name;
        }else{
            $nama_skpd = "All";
        }

        $kelompok_data = KelompokData::findOrFail($get('kelompok_data_id'));
        $data = [
            'kelompok'=> $this->kelompok->all(),
            'activeYear'=> $activeYear,
            'generateUrlYear'=>$generateUrlYear,
            'parents'=>$this->repo->parents(),
            'childs'=>$childs,
            'nilaiOrFormula'=>$nilaiOrFormula,
            'selectSkpd'=>$this->combo->skpd(),
            'get'=>$get,
            'repo'=>$this->repo,
            'nama_skpd'=>$nama_skpd,
            'kelompok_data'=>$kelompok_data,
            'repo'=> $this->repo,
        ];

        //return view('excel_detail',$data);      
        
        Excel::create('New file', function($excel) use ($data) {

            $excel->sheet('First sheet', function($sheet) use ($data) {

                $sheet->loadView('excel_detail' , $data);
            });

        })
        ->download('xls');

    }
}
