<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Models\News;

class BeritaController extends Controller
{

	public function __construct(News $model)
	{
		$this->model = $model;
	}

    public function getIndex()
    {
		$breadcrumbs = [
    		'Berita'=> url('berita'),
    		'Listing'=>'#',
    	];

    	$lists = $this->model
    		->where('status','y')
    		->orderBy('created_at','desc')
    		->paginate(10);
		$sesuatu = "orait";
    	return view('berita',compact('lists','breadcrumbs'));
    }

    public function getRead($slug)
    {
    	$row = $this->model->where('slug',$slug)->firstOrFail();

    	$breadcrumbs = [
    		'Berita'=> url('berita'),
    		$row->judul =>'#',
    	];

    	return view('berita_detail',compact('row','breadcrumbs'));
    }
}
