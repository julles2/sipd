<?php

namespace App\Http\Requests\Velin;

use App\Http\Requests\Request;

class JenisDataRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $this->beforeSave();;

        return [
            'kelompok_data_id'  => 'required|numeric',    
            'jenis_data' => 'required|unique:jenis_datas,jenis_data,'.$this->segment(4),
        
        ];
    }

    public function beforeSave()
    {
        $input = $this->all();

        $input['slug'] = str_slug($input['jenis_data']);

        $this->replace($input);
    }

    public function messages()
    {
        return [
            'kelompok_data_id.required' => 'Kelompok Data is Required',
        ];
    }
}
