<?php

namespace App\Http\Requests\Velin;

use App\Http\Requests\Request;

class ElementRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $this->beforeSave();

        return [
            'kelompok_data_id'  => 'required',
            'jenis_data_id'     => 'required',
            'element'           => 'required'
        ];
    }

    public function messages()
    {
        return [
            'kelompok_data_id.required' => 'Kelompok Data is required',
            'jenis_data_id.required' => 'Jenis Data is required',
        ];
    }

    public function beforeSave()
    {
        $input = $this->all();

        if(empty($input['parent_id']))
        {
            $input['parent_id'] = null;
        }

        $this->replace($input);
    }
}
