<?php

namespace App\Http\Requests\Velin;

use App\Http\Requests\Request;

class KelompokDataRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $this->beforeSave();

        return [
            'kelompok_data' => 'required|unique:kelompok_datas,kelompok_data,'.$this->segment(4),
        ];
    }

    public function beforeSave()
    {
        $input = $this->all();

        $input['slug'] = str_slug($input['kelompok_data']);

        $this->replace($input);
    }
}
