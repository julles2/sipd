<?php
Route::get('tes',function(){
	dd(12);
});
Route::get('/','DashboardController@getIndex');
Route::controller('dashboard','DashboardController');
Route::controller('nilai','NilaiController');
Route::controller('berita','BeritaController');
Route::controller('kegiatan','KegiatanController');
Route::controller('pemda','PemdaController');
Route::controller('link','LinkController');
Route::controller('kontak','KontakController');
Route::controller('login-page','Velin\LoginController');
Route::controller('test','TestController');

if(request()->segment(1) == \Velin::config('backendUrl'))
{
	include __dir__.'/velin_routes.php';	
}