<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Models\ReportingPdf;
use App\Models\Repositories\NilaiRepositories;
class Pdf extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'pdf:generate';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
        $this->model = new ReportingPdf();
        $this->repo = new NilaiRepositories();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        ini_set('max_execution_time', 0);
        $model = $this->model->find(5);
        $peta_kabupaten = !empty($model->peta_kabupaten) ? $model->peta_kabupaten : 'null.png';
        $petaPath = public_path('contents/'.$peta_kabupaten);
        $peta = file_exists($petaPath) ? $petaPath : '';
        
        $kelompok = $model->pdfKelompoks()->first();

        $year = !empty($kelompok->id) ? $kelompok->year : date('Y');
        $html =  view('velin.reporting.pdf.generate',[
                'model'=>$model,
                'peta'=>$peta,
                'repo'=>$this->repo,
                'pdf'=>new \PDF(),
                'year'=>$year,
            ])
            ->render();
        
         \PDF::load($html)
         ->filename(public_path('kimpoy.pdf'))
        ->output();
    }
}
