<?php

namespace App\Jobs;

use App\Jobs\Job;
use App\Models\Element;
use App\Models\Repositories\NilaiRepositories;
use App\Models\User;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Http\Request;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class PdfKecamatan extends Job implements ShouldQueue
{
    use InteractsWithQueue, SerializesModels;

    /**
     * Create a new job instance.
     *
     * @return void
     */

    protected $request;

    public function __construct($request)
    {
        $this->request = $request;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $request   = $this->request;
        $kecamatan = User::whereType("skpd");
        if ($request['kecamatan'] != "all") {
            $kecamatan = $kecamatan->whereId($request['kecamatan']);
        }
        
        $kecamatan = $kecamatan->get();

        // // $kecamatan   = $kecamatan->get();
        $elements = Element::where('jenis_data_id',$request['jenis_data'])->get();

        $pdf = \PDF2::loadView("tes", [
            'kecamatan' => $kecamatan,
            'elements'  => $elements,
            'nilai'     => new NilaiRepositories(),
        ]);

        $pdf->save(public_path('kimpoy.pdf'));
    }
}
