<?php

namespace App\Jobs;

use App\Jobs\Job;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\Models\ReportingPdf;
use App\Models\Repositories\NilaiRepositories;
class Pdf extends Job implements ShouldQueue
{
    use InteractsWithQueue, SerializesModels;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    
    public $model;
    public $repo;

    public function __construct(ReportingPdf $model)
    {
        $this->model = $model;
        $this->repo = new NilaiRepositories();
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $model = $this->model;
        $peta_kabupaten = !empty($model->peta_kabupaten) ? $model->peta_kabupaten : 'null.png';
        $petaPath = public_path('contents/'.$peta_kabupaten);
        $peta = file_exists($petaPath) ? $petaPath : '';
        
        $kelompok = $model->pdfKelompoks()->first();

        $year = !empty($kelompok->id) ? $kelompok->year : date('Y');

        $html =  view('velin.reporting.pdf.generate',[
                'model'=>$model,
                'peta'=>$peta,
                'repo'=>$this->repo,
                'pdf'=>new \PDF(),
                'year'=>$year,
            ])
            ->render();
        
         \PDF::load($html)
         ->filename(public_path('kimpoy.pdf'))
        ->output();
    }
}
