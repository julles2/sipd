<?php namespace App\Repositories;

class Combos
{
	public function kelompokDatas()
	{
		$model = injectModel('KelompokData')->select('id','kelompok_data')
			->orderBy('kelompok_data','asc')
			->lists('kelompok_data','id')->toArray();
	
		return $model;
	}

	public function jenisDatas($kelompok_data_id = "")
	{
		$model = injectModel('JenisData');

		$model = $model->select('id','jenis_data');

			if(!empty($kelompok_data_id))
			{
				$model = $model->whereKelompokDataId($kelompok_data_id);
			}

		$model = $model->lists('jenis_data','id')->toArray();

		return  $model;
	}
	
	
	public function cariAnak($id)
	{
		$model = injectModel('Element')->select('id')->where('parent_id',$id)->get();

		$id = "";

		foreach($model as $row)
		{
			$id.= $row->id.',';

			$id.= $this->cariAnak($row->id);
		}

		return $id;
	}

	public function elementParents($jenis_data_id="" , $id = "")
	{
		$model = injectModel('Element');

		$model = $model->select('id','element');

		if(!empty($jenis_data_id))
		{
			$model = $model->whereJenisDataId($jenis_data_id);
		}

		if(!empty($id))
		{	
			$idNotIn = explode(",",$this->cariAnak($id));

			$model = $model->whereNotIn('id', $idNotIn);

			$model = $model->whereNotIn('id' , [$id]);
		}

		$model = $model->lists('element','id')->toArray();

		return $model;
	}

	// public function parents($jenis_data_id)
	// {
	// 	$model = injectModel('Element')
	// 		->select('id','element')
	// 		->whereParentId(null)
	// 		->whereJenisDataId($jenis_data_id)
	// 		->get('element','id');
			
	// 	$result = [];

	// 	foreach($model as $key => $parent)
	// 	{
	// 		$result += [$key => $parent];
	// 	}

	// 	return $result;
	// }

	public function satuans()
	{
		return injectModel('Satuan')->lists('satuan','id')->toArray();
	}

	public function skpd()
	{
		return injectModel('User')->whereType('skpd')->lists('name','id')->toArray();
	}
}