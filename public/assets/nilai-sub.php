<?php require_once('header.php'); ?>
<?php require_once('sidebar-nilai.php'); ?>

<div class='bc'><i class="fa fa-home"></i> Nilai Profile</div>
<div class='container-fluid'>
	<div class='martop'>
		<div class='row'>
			<div class='col-md-12'>
				<div class="panel panel-default">
					<div class="panel-heading tab-cus">
						<h3 class="panel-title">
							<a href='javascript:void(0);' class='activetab'>2010</a>
							<a href='javascript:void(0);'>2011</a>
							<a href='javascript:void(0);'>2012</a>
							<a href='javascript:void(0);'>2013</a>
							<a href='javascript:void(0);'>2014</a>
							<a href='javascript:void(0);'>2015</a>
							<a href='javascript:void(0);'>2016</a>
							<div class='cl'></div>
						</h3>
					</div>
					<div class="panel-body div-table">
						<table width='100%' cellpadding='0' cellspacing='0' class='table-cus'>
							<tr>
								<th>Nama</th>
								<th width='70'>Nilai</th>
								<th width='100'>Satuan</th>
								<th width='200'>Sumber Daya</th>
								<th width='70'>Grafik</th>
							</tr> 
							<tr>
								<td>1. Administrasi Pemerintahan *</td>
								<td>0</td>
								<td>&nbsp;</td>
								<td>0</td>
								<td align='center'><a href='javascript:void(0);'><i class="fa fa-align-justify"></i> </a></td>
							</tr>
							<tr class='tr1'>
								<td>&nbsp; &nbsp; &nbsp; a. Jumlah Kecamatan</td>
								<td>0</td>
								<td>&nbsp;</td>
								<td>Kecamatan</td>
								<td align='center'><a href='javascript:void(0);'><i class="fa fa-align-justify"></i> </a></td>
							</tr>
							<tr>
								<td>&nbsp; &nbsp; &nbsp; b. Laut 12 Mil dari Darat</td>
								<td>0</td>
								<td>&nbsp;</td>
								<td>Kecamatan</td>
								<td align='center'><a href='javascript:void(0);'><i class="fa fa-align-justify"></i> </a></td>
							</tr>
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<?php require_once('header.php'); ?>
