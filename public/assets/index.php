<?php require_once('header.php'); ?>
<?php require_once('sidebar-dashboard.php'); ?>

<div class='bc'><i class="fa fa-home"></i> Dashboard</div>
<div class='container-fluid'>
	<div class='martop'>
		<div class='row'>
			<div class='col-md-12'>
				<div class="panel panel-default">
					<div class="panel-heading">
						<h3 class="panel-title">Selamat Datang di Sistem Informasi Profil Daerah</h3>
					</div>
					<div class="panel-body">
						Pelaksanaan pembangunan nasional dewasa ini telah mengalami perkembangan yang sangat pesat seiring dengan laju pertumbuhan penduduk dan tingkat kebutuhan hidup masyarakat yang beraneka ragam, sehingga diperlukan peningkatan mutu dan mekanisme pelayanan di semua bidang agar lebih berdaya guna sehingga akan berakibat pada meningkatnya kesejahteraan masyarakat. Tujuan dari kegiatan ini adalah untuk membuat Sistem Informasi Profil Daerah Kota Pasuruan.
					</div>
				</div>
			</div>
		</div>
		<br>
		<div class='row'>
			<div class='col-md-6'>
				<div class="panel panel-default">
					<div class="panel-heading">
						<h3 class="panel-title">Kina *</h3>
					</div>
					<div class="panel-body thumbnail">
						<img src='assets/images/graf.png'>
					</div>
				</div>
			</div>
			<div class='col-md-6'>
				<div class="panel panel-default">
					<div class="panel-heading">
						<h3 class="panel-title">Sekunder</h3>
					</div>
					<div class="panel-body thumbnail">
						<img src='assets/images/graf.png'>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<?php require_once('header.php'); ?>
