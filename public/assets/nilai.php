<?php require_once('header.php'); ?>
<?php require_once('sidebar-nilai.php'); ?>

<div class='bc'><i class="fa fa-home"></i> Nilai Profile</div>
<div class='container-fluid'>
	<div class='martop'>
		<div class='row'>
			<div class='col-md-12'>
				<div class="panel panel-default">
					<div class="panel-heading">
						<h3 class="panel-title">Nilai Profile</h3>
					</div>
					<div class="panel-body div-table">
						<table width='100%' cellpadding='0' cellspacing='0' class='table-cus'>
							<tr>
								<th width='10'>No.</th>
								<th>Kelompok Data</th>
							</tr>
							<tr>
								<td align='center'>I</td>
								<td>
									<b>Data Umum</b><br>
									Jenis Data : <br>
									<ol>
										<li><a href='nilai-sub.php'>Geografi</a></li>
										<li><a href='nilai-sub.php'>Pemerintahan (Administrasi Pemerintahan, Aparatur Negara, Administrasi Kepegawaian)</a></li>
										<li><a href='nilai-sub.php'>Demografi</a></li>
									</ol>
								</td>
							</tr>
							<tr class='tr1'>
								<td align='center'>II</td>
								<td>
									<b>Sosial/Budaya </b><br>
									Jenis Data : <br>
									<ol>
										<li><a href='nilai-sub.php'>Kesehatan</a></li>
										<li><a href='nilai-sub.php'>Pendidikan, Kebudayaan Nasional Pemuda dan Olahraga</a></li>
										<li><a href='nilai-sub.php'>Kesejahteraan Sosial</a></li>
										<li><a href='nilai-sub.php'>Agama</a></li>
									</ol>
								</td>
							</tr>
							<tr>
								<td align='center'>III</td>
								<td>
									<b>Sumber Daya Alam </b><br>
									Jenis Data : <br>
									<ol>
										<li><a href='nilai-sub.php'>Pertanian, Kehutanan, Kelautan, Perikanan, Peternakan, Perkebunan</a></li>
										<li><a href='nilai-sub.php'>Pertambangan dan Energi</a></li>
										<li><a href='nilai-sub.php'>Lingkungan Hidup, Tata Ruang dan Pertanahan</a></li>
									</ol>
								</td>
							</tr>
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<?php require_once('header.php'); ?>
