<?php require_once('header.php'); ?>
<?php require_once('sidebar-berita.php'); ?>

<div class='bc'><i class="fa fa-home"></i> Listing Berita</div>
<div class='container-fluid'>
	<div class='martop'>
		<div class='row'>
			<div class='col-md-6'>
				<div class="panel panel-default"> 
					<div class="panel-body div-table">
						<table width='100%' cellpadding='0' cellspacing='0' class='table-cus'>
							<tr>
								<th width='10'>No.</th>
								<th>Judul Berita</th>
								<th>Deskripsi</th>
							</tr>
							<tr>
								<td align='center'>1.</td>
								<td>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Est a, veniam harum quas, obcaecati quo.</td>
								<td>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Est a, veniam harum quas, obcaecati quo.Lorem ipsum dolor sit amet, consectetur adipisicing elit. Est a, veniam harum quas, obcaecati quo.</td>
							</td>
							<tr class='tr1'>
								<td align='center'>2.</td>
								<td>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Est a, veniam harum quas, obcaecati quo.</td>
								<td>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Est a, veniam harum quas, obcaecati quo.Lorem ipsum dolor sit amet, consectetur adipisicing elit. Est a, veniam harum quas, obcaecati quo.</td>
							</td>
							<tr>
								<td align='center'>3.</td>
								<td>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Est a, veniam harum quas, obcaecati quo.</td>
								<td>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Est a, veniam harum quas, obcaecati quo.Lorem ipsum dolor sit amet, consectetur adipisicing elit. Est a, veniam harum quas, obcaecati quo.</td>
							</td>
						</table>
					</div>
				</div>
			</div>
			<div class='col-md-6'>
				<form method='post' data-parsley-validate>
				<div class="panel panel-default"> 
					<div class="panel-heading">
						<h3 class="panel-title">Sample Form</h3>
					</div>
					<div class="panel-body">
						<div class="input-group">
							<b>Nama</b>
							<input type="text" class="form-control"  required>
						</div>
						<br>
						<div class="input-group">
							<b>Email</b>
							<input type="email" class="form-control" required>
						</div>					
						<br>
						<div class="input-group">
							<b>Komentar</b>
							<textarea class='form-control' required rows='3' cols='30'></textarea>
						</div> 
					</div>
					<div class="panel-footer">
						<button type='submit' class='btn btn-md btn-primary'>Submit</button>
						&nbsp;
						<button type='reset' class='btn btn-md btn-default'>Reset</button>
					</div>
				</div>
				</form>
			</div>
		</div>
	</div>
</div>
<?php require_once('header.php'); ?>
