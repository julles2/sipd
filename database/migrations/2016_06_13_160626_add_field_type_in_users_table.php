<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddFieldTypeInUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->enum('type',['admin','skpd'])->default('admin');
        });

        \DB::table('users')
            ->insert([
                'role_id'   => 2,
                'name'      => 'Admin Kecamatan Plumbon',
                'username'  => 'plumbon',
                'password'  => \Hash::make('plumbon'),
                'email'     => 'plumbon@sipd.com',
                'type'      => 'skpd',
            ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->dropColumn('type');
        });
    }
}
