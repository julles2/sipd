<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePemdasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pemdas', function (Blueprint $table) {
            $table->increments('id');
            $table->string('judul');
            $table->text('description');
            $table->enum('type',['bappeda','sipd']);
            $table->integer('order')->index();
            $table->enum('status',['y','n']);
            $table->string('slug')->index();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('pemdas');
    }
}
