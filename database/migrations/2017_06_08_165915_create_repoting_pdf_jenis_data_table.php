<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRepotingPdfJenisDataTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('reporting_pdf_jenis_data', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('kelompok_pdf_id')->unsigned();
            $table->integer('jenis_data_id')->unsigned();
            $table->timestamps();

            $table->foreign('kelompok_pdf_id')
                ->references('id')
                ->on('reporting_pdf_kelompok_datas')
                ->onUpdate('cascade')
                ->onDelete('restrict');

            $table->foreign('jenis_data_id')
                ->references('id')
                ->on('jenis_datas')
                ->onUpdate('cascade')
                ->onDelete('restrict');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('reporting_pdf_jenis_data');
    }
}
