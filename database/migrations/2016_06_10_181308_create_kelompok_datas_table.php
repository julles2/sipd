<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateKelompokDatasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('kelompok_datas', function (Blueprint $table) {
            $table->increments('id');
            $table->string('kelompok_data',255);
            $table->string('slug',300)->index();
            $table->timestamps();
        });

        \DB::table('kelompok_datas')->insert([
            'id'    => 1,
            'kelompok_data' => 'Data Umum',
            'slug'  => 'data-umum',
        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('kelompok_datas');
    }
}
