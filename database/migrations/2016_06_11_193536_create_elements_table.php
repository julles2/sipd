<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateElementsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('elements', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('jenis_data_id')->unsigned();
            $table->integer('satuan_id')->unsigned();
            $table->integer('parent_id')->unsigned()->nullable();
            
            $table->foreign('jenis_data_id')->references('id')->on('jenis_datas')
                ->onUpdate('cascade')
                ->onDelete('restrict');

            $table->foreign('satuan_id')->references('id')->on('satuans')
                ->onUpdate('cascade')
                ->onDelete('restrict');

            $table->string('element');

            $table->timestamps();

            $table->foreign('parent_id')->references('id')->on('elements')
                ->onUpdate('cascade')
                ->onDelete('restrict');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('elements' , function($table){
            $table->dropForeign('elements_jenis_data_id_foreign','elements_parent_id_foreign','elements_satuan_id_foreign');
        });

        Schema::drop('elements');
    }
}
