<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateNilaisTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('nilais', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('user_id')->unsigned();
            
            $table->foreign('user_id')->references('id')->on('users')
                ->onUpdate('cascade')
                ->onDelete('cascade');
            
            $table->integer('element_id')->unsigned();

            $table->foreign('element_id')->references('id')->on('elements')
                ->onUpdate('cascade')
                ->onDelete('cascade');
            
            $table->string('sumber_data',300);

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('nilais' , function($table){
            $table->dropForeign('nilais_element_id_foreign');
            $table->dropForeign('nilais_user_id_foreign');
        });
        Schema::drop('nilais');
    }
}
