<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateReportingPdfKelompokDatasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('reporting_pdf_kelompok_datas', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('pdf_id')->unsigned();
            $table->integer('kelompok_data_id')->unsigned();
            $table->text('narasi');
            $table->string('year',4);
            $table->timestamps();

            $table->foreign('pdf_id')
                ->references('id')
                ->on('reporting_pdf')
                ->onUpdate('cascade')
                ->onDelete('restrict');

            $table->foreign('kelompok_data_id')
                ->references('id')
                ->on('kelompok_datas')
                ->onUpdate('cascade')
                ->onDelete('restrict');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('reporting_pdf_kelompok_datas');
    }
}
