<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateJenisDatasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('jenis_datas', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('kelompok_data_id')->unsigned();
            $table->foreign('kelompok_data_id')->references('id')
                ->on('kelompok_datas')
                ->onUpdate('cascade')
                ->onDelete('restrict');
            $table->string('jenis_data');
            $table->string('slug',300)->index();
            $table->timestamps();
        });

        \DB::table('jenis_datas')->insert([
            'kelompok_data_id'  => 1,
            'jenis_data'    => 'Geografi',
            'slug'  => 'geografi',
        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('jenis_datas' , function($table){
            $table->dropForeign('jenis_datas_kelompok_data_id_foreign');
        });
        Schema::drop('jenis_datas');
    }
}
