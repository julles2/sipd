<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateReportingPdfsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('reporting_pdf', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id')->unsigned()->nullable();

            $table->string('cover_judul');

            $table->longText('kata_pengantar');

            $table->string('peta_kabupaten',100);

            $table->longText('sekilas_data_kecamatan');
            $table->longText('sekilas_layanan_kecamatan');
            $table->longText('sekilas_kontak_kecamatan');

            // daftar isi di skip dulu
                // here
            //

            
            $table->timestamps();


            $table->foreign('user_id')
                ->references('id')
                ->on('users')
                ->onUpdate('cascade')
                ->onDelete('restrict');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('reporting_pdf');
    }
}
