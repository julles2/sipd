<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddFieldEndYearOnNilaisTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('reporting_pdf_kelompok_datas', function (Blueprint $table) {
            $table->string('end_year',4);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('reporting_pdf_kelompok_datas', function (Blueprint $table) {
            $table->dropColumn('end_year');
        });
    }
}
