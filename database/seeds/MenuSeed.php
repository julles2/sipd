<?php
/**
 * Example :
 *
 * Generate Parent Menu:
 *     \Velin::addMenu([
            'parent_id'     => null,
            'title'         => 'Development',
            'order'         => 100,
            'controller'    => '#',
            'slug'          => 'development',
        ],[]);

    Generate Child Menu : 
        \Velin::addMenu([
                'parent_id'     => 'development',
                'title'         => 'Action',
                'order'         => 1,
                'controller'    => 'ActionController',
                'slug'          => 'action',
            ],['index','update','delete','create']);

    Updating Menu : 

        \Velin::updateMenu([
                'parent_id'     => 'development',
                'title'         => 'Action',
                'order'         => 1,
                'controller'    => 'ActionController',
                'slug'          => 'action',
            ],['index','update','delete','create']);

    Delete Menu : 
        \Velin::deleteMenu('slug');
 */


use Illuminate\Database\Seeder;

class MenuSeed extends Seeder
{
    public function run()
    {
        // Start Development
    	\Velin::addMenu([
			'parent_id'		=> null,
			'title'			=> 'Development',
			'order'			=> 100,
			'controller'	=> '#',
			'slug'			=> 'development',
		],[]);

            \Velin::addMenu([
                'parent_id'     => 'development',
                'title'         => 'Action',
                'order'         => 1,
                'controller'    => 'ActionController',
                'slug'          => 'action',
            ],['index','update','delete','create']);

        // end development
        
        // start users
        \Velin::addMenu([
            'parent_id'     => null,
            'title'         => 'Manage User',
            'order'         => 1,
            'controller'    => '#',
            'slug'          => 'manage-user',
        ],[]);

            \Velin::addMenu([
                'parent_id'     => 'manage-user',
                'title'         => 'Role',
                'order'         => 1,
                'controller'    => 'RoleController',
                'slug'          => 'role',
            ],['index','update','delete','create','view']);

            \Velin::addMenu([
                'parent_id'     => 'manage-user',
                'title'         => 'User',
                'order'         => 2,
                'controller'    => 'UserController',
                'slug'          => 'user',
            ],['index','update','delete','create']);

        // 
        
        // Start Master Data
        \Velin::addMenu([
            'parent_id'     => null,
            'title'         => 'Master Data',
            'order'         => 2,
            'controller'    => '#',
            'slug'          => 'master-data',
        ],[]);

             \Velin::addMenu([
                'parent_id'     => 'master-data',
                'title'         => 'Kelompok Data',
                'order'         => 1,
                'controller'    => 'KelompokDataController',
                'slug'          => 'kelompok-data',
            ],['index','update','delete','create']); 

            \Velin::addMenu([
                'parent_id'     => 'master-data',
                'title'         => 'Jenis Data',
                'order'         => 2,
                'controller'    => 'JenisDataController',
                'slug'          => 'jenis-data',
            ],['index','update','delete','create']); 

            \Velin::addMenu([
                'parent_id'     => 'master-data',
                'title'         => 'Satuan',
                'order'         => 3,
                'controller'    => 'SatuanController',
                'slug'          => 'satuan',
            ],['index','update','delete','create']); 

            \Velin::addMenu([
                'parent_id'     => 'master-data',
                'title'         => 'Element',
                'order'         => 4,
                'controller'    => 'ElementController',
                'slug'          => 'element',
            ],['index','update','delete','create']); 
        //
        
        // start keterisian data
        
        \Velin::addMenu([
            'parent_id'     => null,
            'title'         => 'Keterisian Data',
            'order'         => 3,
            'controller'    => '#',
            'slug'          => 'keterisian-data',
        ],[]);

            \Velin::addMenu([
                'parent_id'     => 'keterisian-data',
                'title'         => 'Nilai',
                'order'         => 1,
                'controller'    => 'NilaiController',
                'slug'          => 'nilai',
            ],['index']); 

        // 
         
        // Start Konten 
        
        \Velin::addMenu([
            'parent_id'     => null,
            'title'         => 'Konten',
            'order'         => 4,
            'controller'    => '#',
            'slug'          => 'konten',
        ],[]);

            \Velin::addMenu([
                'parent_id'     => 'konten',
                'title'         => 'Header',
                'order'         => 1,
                'controller'    => 'HeaderController',
                'slug'          => 'konten-header',
            ],['index']); 

            \Velin::addMenu([
                'parent_id'     => 'konten',
                'title'         => 'Intro',
                'order'         => 2,
                'controller'    => 'IntroController',
                'slug'          => 'konten-intro',
            ],['index']);

            \Velin::addMenu([
                'parent_id'     => 'konten',
                'title'         => 'Pemda',
                'order'         => 3,
                'controller'    => 'PemdaController',
                'slug'          => 'konten-pemda',
            ],['index','update','delete','create']);

            \Velin::addMenu([
                'parent_id'     => 'konten',
                'title'         => 'Berita',
                'order'         => 4,
                'controller'    => 'BeritaController',
                'slug'          => 'konten-berita',
            ],['index','update','delete','create']);    

            \Velin::addMenu([
                'parent_id'     => 'konten',
                'title'         => 'Link',
                'order'         => 6,
                'controller'    => 'LinkController',
                'slug'          => 'konten-link',
            ],['index','update','delete','create']);

            \Velin::addMenu([
                'parent_id'     => 'konten',
                'title'         => 'Album Foto',
                'order'         => 7,
                'controller'    => 'FotoController',
                'slug'          => 'konten-album',
            ],['index','update','delete','create','view']);   

            \Velin::addMenu([
                'parent_id'     => 'konten',
                'title'         => 'Album Video',
                'order'         => 7,
                'controller'    => 'VideoController',
                'slug'          => 'video-album',
            ],['index','update','delete','create','view']);

            \Velin::updateMenu([
                'parent_id'     => 'konten',
                'title'         => 'Album Video',
                'order'         => 7,
                'controller'    => 'VideoController',
                'slug'          => 'video-album',
            ],['index','update','delete','create']);


             //Start Kontak
    
            \Velin::addMenu([
                'parent_id'     => null,
                'title'         => 'Kontak',
                'order'         => 5,
                'controller'    => '#',
                'slug'          => 'kontak',
            ],[]);

                \Velin::addMenu([
                    'parent_id'     => 'kontak',
                    'title'         => 'Informasi',
                    'order'         => 1,
                    'controller'    => 'InformasiController',
                    'slug'          => 'kontak-informasi',
                ],['index']);


            //
            
            // Start Reporting

            \Velin::addMenu([
                'parent_id'     => null,
                'title'         => 'Reporting',
                'order'         => 5,
                'controller'    => '#',
                'slug'          => 'reporting',
            ],[]);

                \Velin::addMenu([
                    'parent_id'     => 'reporting',
                    'title'         => 'PDF',
                    'order'         => 1,
                    'controller'    => 'ReportingPdfController',
                    'slug'          => 'reporting-pdf',
                ],['index','update','delete','create','view']);

                \Velin::addMenu([
                    'parent_id'     => 'reporting',
                    'title'         => 'Kecamatan',
                    'order'         => 2,
                    'controller'    => 'ReportingKecamatanController',
                    'slug'          => 'reporting-kecamatan',
                ],['index']);

            //    
    }

    //

   
}
