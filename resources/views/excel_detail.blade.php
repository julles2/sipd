<table>
	<tr>
		<td>Nama SKPD : {{ $nama_skpd }}</td>
	</tr>
	<tr>
		<td>Kelompok Data : {{ $kelompok_data->kelompok_data }}</td>
	</tr>
	

</table>
@foreach($kelompok_data->jenisDatas as $jenis)
						<table width='100%' cellpadding='0' cellspacing='0' class='table-cus'>
							<tr>
								<th colspan="10" style="background-color:#FFFF00;">Jenis Data : {{ $jenis->jenis_data }}</th>
							</tr>
							<tr>
								<th>Nama</th>
								<?php
								$awal = date("Y") - 6;
								?>
								@for($a=$awal;$a<=date('Y');$a++)
								<th>{{  $a }}</th>
								@endfor		
								<th width='100'>Satuan</th>
								<th width='200'>Sumber Data</th>

							</tr> 
							<!--tr>
								<td>1. Administrasi Pemerintahan *</td>
								<td>0</td>
								<td>&nbsp;</td>
								<td>0</td>
								<td align='center'><a href='javascript:void(0);'><i class="fa fa-align-justify"></i> </a></td>
							</tr>
							<tr class='tr1'>
								<td>&nbsp; &nbsp; &nbsp; a. Jumlah Kecamatan</td>
								<td>0</td>
								<td>&nbsp;</td>
								<td>Kecamatan</td>
								<td align='center'><a href='javascript:void(0);'><i class="fa fa-align-justify"></i> </a></td>
							</tr>
							<tr>
								<td>&nbsp; &nbsp; &nbsp; b. Laut 12 Mil dari Darat</td>
								<td>0</td>
								<td>&nbsp;</td>
								<td>Kecamatan</td>
								<td align='center'><a href='javascript:void(0);'><i class="fa fa-align-justify"></i> </a></td>
							</tr-->
							<?php
							$no=0;
							$parents = $repo->parents($jenis->id);
							?>
							@foreach($parents as $parent)
							@php($no++)
							<tr>
								<td>{{$no}}. {{$parent->element}} {{ $parent->status }}</td>
								@for($a=$awal;$a<=date('Y');$a++)
								<td>{{ $nilaiOrFormula($parent,$a)['nilai'] }}</td>
								@endfor		
								<td>{{ $parent->satuan->satuan }}</td>
								<td>{{ $nilaiOrFormula($parent,date('Y'))['sumber_data'] }}</td>
							</tr>
							<?= $childs($parent, $no,request()->get('year'),request()->get('skpd_id')) ?>
							@endforeach
						</table>
@endforeach	