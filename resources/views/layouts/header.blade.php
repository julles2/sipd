<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">

<head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>SIPD BAPPEDA</title>
    <link href="{{ asset(null) }}assets/css/bootstrap.css" rel="stylesheet" />
    <link href="{{ asset(null) }}assets/css/font-awesome.css" rel="stylesheet" />
    <link href="{{ asset(null) }}assets/css/bappeda.css" rel="stylesheet" />
    <link rel="stylesheet" href="{{ asset(null) }}velin/fancybox/source/jquery.fancybox.css?v=2.1.5" type="text/css" media="screen" /><!--[if lte IE 8]><script language="javascript" type="text/javascript" src="{{ asset(null) }}velin/vendors/flot/excanvas.min.js"></script><![endif]-->
    
    <script src="{{ asset(null) }}velin/vendors/jquery-1.9.1.js"></script>
    <script src="{{ asset(null) }}assets/js/bootstrap.min.js"></script>
    <script src="http://code.highcharts.com/highcharts.js"></script>
    <script type="text/javascript" src="{{asset(null)}}velin/fancybox/lib/jquery.mousewheel-3.0.6.pack.js"></script>
    <script type="text/javascript" src="{{asset(null)}}velin/fancybox/source/jquery.fancybox.pack.js?v=2.1.5"></script>
    
</head>

<body class='body'>
	<div class='header'>
		<div class='container row'>
			<div class='left' align='center' style='width:%;'>
				<img src='{{ asset('contents/'.findHeader()->logo) }}'>
			</div>
			<div class='right'>
				<h3>{{ strtoupper(findHeader()->judul) }}</h3>
				<h4>{{ findHeader()->judul_kota }}</h4>
			</div>
			<div class='cl'></div>
		</div>
	</div>
	
	