	<div class='sidebar'>
		@include('layouts.menu')
		<div class="secNav"> 
			<div align='center' class='headpage'><i class="fa fa-dashboard"></i> Nilai Profil</div> 
			<div class='menuna'>
			@foreach($kelompok as $kel)
				<a href='#{{ $kel->slug }}' data-toggle="collapse">{{ $kel->kelompok_data }}<span class="label label-danger right">{{ $kel->jenisDatas()->count() }}</span></a>
				<div id="{{ $kel->slug }}" class="collapse inmenuna">
					@foreach($kel->jenisDatas as $jenis)

						@php($strUrl = "nilai/detail?kelompok_data_id=".$kel->id."&jenis_data_id=".$jenis->id."&year=2010&skpd_id=")

						<a href='{{ url($strUrl) }}'>{{ $jenis->jenis_data }}</a>
					@endforeach
				</div>				
			@endforeach
				<!--a href='#menusosbud' data-toggle="collapse">Sosial / Budaya <span class="label label-danger right">4</span></a>
				<div id="menusosbud" class="collapse inmenuna">
					<a href='nilai-sub.php'>Geografi</a>
				</div>				

				<a href='#menubla' data-toggle="collapse">Menu Bla <span class="label label-danger right">4</span></a>
				<div id="menubla" class="collapse inmenuna">
					<a href='nilai-sub.php'>Test Bla</a>
					<a href='nilai-sub.php'>Test Bla</a>
					<a href='nilai-sub.php'>Test Bla</a>
				</div-->				
			</div>
			
			<div class="clear"></div>
		</div>		
	</div>
	<div class='content'>