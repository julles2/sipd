<!DOCTYPE html>
<html>
<head>
	<title></title>
	{{-- <link rel="stylesheet" href="{{ asset('velin/pdf.css') }}"> --}}
<style>
	.customers {
    font-family: "Trebuchet MS", Arial, Helvetica, sans-serif;
    border-collapse: collapse;
    width: 100%;
}

.customers td, .customers th {
    border: 1px solid #ddd;
    padding: 8px;
}

.customers tr:nth-child(even){background-color: #f2f2f2;}

.customers tr:hover {background-color: #ddd;}

.customers th {
    padding-top: 12px;
    padding-bottom: 12px;
    text-align: left;
    background-color: #4CAF50;
    color: white;
}
</style>
</head>

<body>

		
		@foreach($elements as $element)
				<div style="page-break-after:always;">
					<p>&nbsp;</p>
				<table class="customers" border="1">
					<thead>
						<tr>
							<th colspan="100">{{ $element->element }} - ({{ $element->satuan->satuan }})</th>
						</tr>
					</thead>
					<tr>
						<td><b>NO</b></td>
						<td><b>Nama Kecamatan</b></td>
						<td colspan="100"><b>Tahun</b></td>
					</tr>
					<tr>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						@for($a=2012;$a<date("Y");$a++)
							<td>{{ $a }}</td>
						@endfor
					</tr>
					<?php $no = 0; ?>
					@foreach($kecamatan as $k)
					<?php $no++; ?>
							
						<tr>
							<td>{{ $no }}</td>
							<td>{{ $k->name }}</td>
							@for($a=2012;$a<date("Y");$a++)
								<td>
									<?php
										$hasil = $nilai->nilaiOrFormula($element, $a, $k->id)['nilai'];
										if(empty($hasil))
										{
											$hasil = "0.00";
										}
										echo $hasil;
									?>
								</td>
							@endfor
						</tr>
					@endforeach
				</table>
			</div>
		@endforeach

		
</body>
</html>