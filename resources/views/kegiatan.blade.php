@extends('layouts.layout')
@section('content')
@include('layouts.sidebar_berita')

<div class='bc'><i class="fa fa-home"></i>
{!! breadcrumbs($breadcrumbs) !!}
</div>
<div class='container-fluid'>
	<div class='martop'>
		<div class='row'>
			<div class='col-md-12'>
				<form method='post' data-parsley-validate>
				<div class="panel panel-default"> 
					<div class="panel-heading">
						<h3 class="panel-title">Kegiatan Album Foto</h3>
					</div>
					<div class="panel-body">
						<div class="col-md-offset-2 col-md-8">
                            <div class="row">
                            @foreach($fotos as $foto)
                                <a class="col-sm-4" href = "{{ url('kegiatan/detail/'.$foto->slug) }}">
                                    <img class="img-responsive" src="{{ asset('contents/'.$foto->image) }}" style="height:200px;width:200px;">
                                	<p style="text-align:justify;">{{ $foto->judul }}</p>
                                </a>
                            @endforeach
                            </div>
                            
                        </div>
					</div>
				</div>
				</form>
			</div>
			
		</div>
	</div>
</div>

<div class='container-fluid'>
	<div class='martop'>
		<div class='row'>
			<div class='col-md-12'>
				<form method='post' data-parsley-validate>
				<div class="panel panel-default"> 
					<div class="panel-heading">
						<h3 class="panel-title">Kegiatan Album Video</h3>
					</div>
					<div class="panel-body">
						<div class="col-md-offset-2 col-md-8">
                            <div class="row">
                            @foreach($videos as $video)
                                <a data-fancybox-type="iframe" id = 'box' class="col-sm-4" href = "http://www.youtube.com/embed/{{ $video->youtube_id }}?enablejsapi=1&wmode=opaque">
                                    <img class="img-responsive" src="http://img.youtube.com/vi/{{ $video->youtube_id }}/0.jpg" style="height:200px;width:200px;">
                                	<p style="text-align:justify;">{{ $video->judul }}</p>
                                </a>
                            @endforeach
                            </div>
                            
                        </div>
					</div>
				</div>
				</form>
			</div>
			
		</div>
	</div>
</div>
@endsection
@section('script')
<script type="text/javascript">
	$(document).ready(function(){
	
		$("#box").fancybox({
          //maxWidth  : 800,
          //maxHeight : 800,
          fitToView : false,
          //width   : '100%',
          //height    : '100%',
          autoSize  : true,
          closeClick  : false,
          openEffect  : 'elastic',
          closeEffect : 'elastic'
        });

	});
</script>
@endsection