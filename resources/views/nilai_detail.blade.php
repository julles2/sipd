@extends('layouts.layout')
@section('content')
@include('layouts.sidebar_nilai')

<div class='bc'><i class="fa fa-home"></i> Nilai Profile</div>
<div class='container-fluid'>
	<div class='martop'>
		
		<div class="row">
			<div class='col-md-12'>
				<form method='post' data-parsley-validate>
				<div class="panel panel-default"> 
					<div class="panel-heading">
						<h3 class="panel-title">Filter</h3>
					</div>
					<div class="panel-body">
						<div class="form-group">
							<b>SKPD</b>
							{!! Form::select('skpd_id',[''=>'All'] + $selectSkpd,request()->get('skpd_id'),['class'=>'form-control','id'=>'skpd_id']) !!}
						</div>
						<div class="form-group">
						<?php
						$urlExcel = url("nilai/excel?kelompok_data_id=".$get('kelompok_data_id')."&jenis_data_id=".$get('jenis_data_id')."&year=".$get('year')."&skpd_id=".$get('skpd_id'));
						?>
						<a href = "{{ $urlExcel }}">
								<img src="{{ asset('excel.png') }}" width="25" height="25">
								<br/>
								<small>Export To Excel</small>
						</a>
						</div>
					</div>
				</div>
				</form>
			</div>
		</div>

		<div class='row'>
			<div class='col-md-12'>
				<div class="panel panel-default">
					<div class="panel-heading tab-cus">
						<h3 class="panel-title">
							<!--a href='javascript:void(0);' class='activetab'>2010</a-->
							@for($a=2010;$a<=date('Y');$a++)
							
								<a href='{{ $generateUrlYear($a) }}' {{ $activeYear($a) }}  >{{ $a }}</a>
							
							@endfor
							
							<div class='cl'></div>
						</h3>
					</div>
					<div class="panel-body div-table">
						<table width='100%' cellpadding='0' cellspacing='0' class='table-cus'>
							<tr>
								<th>Nama</th>
								<th width='70'>Nilai</th>
								<th width='100'>Satuan</th>
								<th width='200'>Sumber Data</th>
								<th width='70'>Grafik</th>
							</tr> 
							<!--tr>
								<td>1. Administrasi Pemerintahan *</td>
								<td>0</td>
								<td>&nbsp;</td>
								<td>0</td>
								<td align='center'><a href='javascript:void(0);'><i class="fa fa-align-justify"></i> </a></td>
							</tr>
							<tr class='tr1'>
								<td>&nbsp; &nbsp; &nbsp; a. Jumlah Kecamatan</td>
								<td>0</td>
								<td>&nbsp;</td>
								<td>Kecamatan</td>
								<td align='center'><a href='javascript:void(0);'><i class="fa fa-align-justify"></i> </a></td>
							</tr>
							<tr>
								<td>&nbsp; &nbsp; &nbsp; b. Laut 12 Mil dari Darat</td>
								<td>0</td>
								<td>&nbsp;</td>
								<td>Kecamatan</td>
								<td align='center'><a href='javascript:void(0);'><i class="fa fa-align-justify"></i> </a></td>
							</tr-->
							@php($no=0)
							@foreach($parents as $parent)
							@php($no++)
							<tr>
								<td>{{$no}}. {{$parent->element}} {{ $parent->status }}</td>
								<td>{{ $nilaiOrFormula($parent)['nilai'] }}</td>
								<td>{{ $parent->satuan->satuan }}</td>
								<td>{{ $nilaiOrFormula($parent)['sumber_data'] }}</td>
								<td align='center'><a href='{{ $repo->generateUrlChart($parent->id) }}'><i class="fa fa-align-justify"></i> </a></td>
							</tr>
							<?= $childs($parent, $no,request()->get('year'),request()->get('skpd_id')) ?>
							@endforeach
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection
@section('script')
<script type="text/javascript">
	
	$(document).ready(function(){
		$("#skpd_id").on('change',function(){
			val = $(this).val();
			kelompok_data_id = '?kelompok_data_id={{ $get("kelompok_data_id") }}';
			jenis_data_id = '&jenis_data_id={{ $get("jenis_data_id") }}';
			year = '&year={{ $get("year") }}';
			skpd_id = '&skpd_id=' + val;

			strUrl = '{{ url("nilai/detail") }}'+kelompok_data_id + jenis_data_id + year + skpd_id;

			document.location.href=strUrl;
		});
	});	

</script>
@endsection