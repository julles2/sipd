
        <!--/.fluid-container-->
        <link href="{{ asset(null) }}velin/vendors/datepicker.css" rel="stylesheet" media="screen">
        <link href="{{ asset(null) }}velin/vendors/uniform.default.css" rel="stylesheet" media="screen">
        <link href="{{ asset(null) }}velin/vendors/chosen.min.css" rel="stylesheet" media="screen">

        <link href="{{ asset(null) }}velin/vendors/wysiwyg/bootstrap-wysihtml5.css" rel="stylesheet" media="screen">

        <script src="https://code.jquery.com/jquery-1.12.4.min.js"></script>
        <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
    
        <script src="{{ asset(null) }}velin/bootstrap/js/bootstrap.min.js"></script>
        <script src="{{ asset(null) }}velin/vendors/jquery.uniform.min.js"></script>
        <script src="{{ asset(null) }}velin/vendors/chosen.jquery.min.js"></script>
        <script src="{{ asset(null) }}velin/vendors/bootstrap-datepicker.js"></script>

        <script src="{{ asset(null) }}velin/vendors/wysiwyg/wysihtml5-0.3.0.js"></script>
        <script src="{{ asset(null) }}velin/vendors/wysiwyg/bootstrap-wysihtml5.js"></script>

        <script src="{{ asset(null) }}velin/vendors/wizard/jquery.bootstrap.wizard.min.js"></script>
        
        <script src="{{ asset(null) }}velin/assets/scripts.js"></script>
        
        <script src="//cdn.datatables.net/1.10.7/js/jquery.dataTables.min.js"></script>
        
        <script type="text/javascript" src = "https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert-dev.min.js"></script>
        
        <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/js/select2.min.js"></script>
        
        <script type="text/javascript" src="{{asset(null)}}velin/fancybox/lib/jquery.mousewheel-3.0.6.pack.js"></script>
        <script type="text/javascript" src="{{asset(null)}}velin/fancybox/source/jquery.fancybox.pack.js?v=2.1.5"></script>
        <script src="{{ asset(null) }}ckeditor/ckeditor.js"></script>
        <script src="//cdnjs.cloudflare.com/ajax/libs/lity/2.2.2/lity.min.js"></script>
        <script src="//cdnjs.cloudflare.com/ajax/libs/bootstrap-multiselect/0.9.15/js/bootstrap-multiselect.min.js"></script>
        @yield('script')
        @stack('scripts')
    </body>

</html>