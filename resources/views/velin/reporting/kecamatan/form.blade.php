@extends('velin.layouts.layout')
@section('content')
<div class="container-fluid">
    <div class="row-fluid">
        <div class="span12" id="content">
              <!-- morris stacked chart -->
            <div class="row-fluid">
                <!-- block -->
                <div class="block">
                    <div class="navbar navbar-inner block-header">
                        <div class="muted pull-left">{{ Velin::labelAction() }}</div>
                    </div>
                    <div class="block-content collapse in">
                        <div class="span12">
                             {!! Form::open(['class'=>'form-horizontal']) !!}
                              <fieldset>
                                <div class="control-group">
                                  <label class="control-label" for="focusedInput">Kecamatan</label>
                                  <div class="controls">
                                    {!! Form::select('kecamatan',$kecamatan,null,['class' => 'input-xxlarge']) !!}
                                  </div>
                                </div>

                                <div class="control-group">
                                  <label class="control-label" for="focusedInput">Kelompok Data</label>
                                  <div class="controls">
                                    {!! Form::select('kelompok_data',$kelompok_data,null,['class' => 'input-xxlarge','id'=>'kelompok_data']) !!}
                                  </div>
                                </div>

                                <div class="control-group">
                                  <label class="control-label" for="focusedInput">Jenis Data</label>
                                  <div class="controls">
                                    {!! Form::select('jenis_data',$jenis_data,null,['class' => 'input-xxlarge','id'=>'jenis_data']) !!}
                                  </div>
                                </div>
								                <div class="control-group">
                                  <label class="control-label" for="focusedInput">Element</label>
                                  <div class="controls">
                                    {!! Form::select('elements[]',$element,null,['class' => 'input-xxlarge select2','id'=>'elementSelect','style'=>'font-size:12px;','multiple']) !!}
                                  </div>
                                </div>

                                <div class="control-group">
                                  <label class="control-label" for="focusedInput">Dari</label>
                                  <div class="controls">
                                    {!! Form::selectRange('dari',2010,date("Y"),null,['class' => 'input-xxlarge']) !!}
                                  </div>
                                </div>

                                <div class="control-group">
                                  <label class="control-label" for="focusedInput">Sampai</label>
                                  <div class="controls">
                                    {!! Form::selectRange('sampai',2010,date("Y"),null,['class' => 'input-xxlarge']) !!}
                                  </div>
                                </div>
                                
                                <div class="form-actions">
                                  <button type="submit" class="btn btn-primary">Export</button>
                                  {{-- <button type="reset" class="btn">Cancel</button> --}}
                                </div>
                              </fieldset>
                            {!! Form::close() !!}

                        </div>
                    </div>
                </div>
                <!-- /block -->
            </div>
        </div>
    </div>
    <hr>
    <footer>
        <p>{{ Velin::config('footer') }}</p>
    </footer>
</div>
@endsection
@push('scripts')
<script type="text/javascript">
	$(document).ready(function(){
		$(".select2").select2();
		$("#kelompok_data").on('change',function(){
			$kelompokData = $(this).val();
			$url = "{{ Velin::urlBackendAction('jenis-data') }}/" + $kelompokData;
			$.ajax({
				type: 'get',
				url: $url,
				success: function(res){
					$html = "";
					$.each(res.results,function(id,jenis_data){
						$html += "<option value = '"+ id +"'>"+jenis_data+"</option>";
					})

					$("#jenis_data").html($html);
				}
			});
		});

		$("#jenis_data").on('change',function(){
			$jenisData = $(this).val();
			$url = "{{ Velin::urlBackendAction('element') }}/" + $jenisData;
			$.ajax({
				type: 'get',
				url: $url,
				success: function(res){
					$html = "";
					$.each(res.results,function(id,jenis_data){
						$html += "<option value = '"+ id +"'>"+jenis_data+"</option>";
					})
					
					$("#elementSelect").html($html);
				}
			});
		});
	});
</script>
@endpush

@include('velin.common.validation')