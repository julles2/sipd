
<!DOCTYPE html>
<html>
<head>
	<title>{{ $model->cover_judul }}</title>
	{{-- <link rel="stylesheet" href="{{ asset('velin/pdf.css') }}"> --}}
<style>
	.customers {
    font-family: "Trebuchet MS", Arial, Helvetica, sans-serif;
    border-collapse: collapse;
    width: 100%;
}

.customers td, .customers th {
    border: 1px solid #ddd;
    padding: 8px;
}

.customers tr:nth-child(even){background-color: #f2f2f2;}

.customers tr:hover {background-color: #ddd;}

.customers th {
    padding-top: 12px;
    padding-bottom: 12px;
    text-align: left;
    background-color: #4CAF50;
    color: white;
}
</style>
</head>

<body>

<div style="page-break-after:always;">

@include("velin.reporting.pdf.generate.cover")

</div>


<div style="page-break-after:always;">
    @include("velin.reporting.pdf.generate.kata_pengantar")
</div>

<div style="page-break-after:always;">
@include("velin.reporting.pdf.generate.peta_kabupaten")
</div>

<div style="page-break-after:always;">
@include("velin.reporting.pdf.generate.sekilas_kecamatan")
</div>


<div style="page-break-after:always;">
    @include("velin.reporting.pdf.generate.daftar_isi")
</div>


<div style="page-break-after:always;">
@include("velin.reporting.pdf.generate.nilai")
</div>

</body>
</html>