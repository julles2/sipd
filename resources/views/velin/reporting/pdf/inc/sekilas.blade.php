<div id="sekilas_tabs">
	<ul>
	  <li><a href="#sekilas-1">Sekilas Kecamatan</a></li>
	  <li><a href="#sekilas-2">Layanan</a></li>
	  <li><a href="#sekilas-3">Kontak Kecamatan</a></li>
	</ul>
	<div id = "sekilas-1">
		{!! Form::textarea('sekilas_data_kecamatan',null,['class'=>'ckeditor']) !!}
	</div>
	<div id = "sekilas-2">
		{!! Form::textarea('sekilas_layanan_kecamatan',null,['class'=>'ckeditor']) !!}
	</div>
	<div id = "sekilas-3">
		{!! Form::textarea('sekilas_kontak_kecamatan',null,['class'=>'ckeditor']) !!}
	</div>
</div>