@if(!empty($model->id))
	<a href = "{{ Velin::urlBackendAction("pop-nilai/".$model->id) }}" class="btn btn-success" style="color:white;" data-lity> Add </a>
	<br/>
	<table class="table" style="border-collapse:collapse;margin-top:10px;" border="1">
	      <thead>
	        <tr>
	          <th width="">Kelompok Data</th>
	          <th width="">Action</th>
	        </tr>
	      </thead>
	      <tbody>
	      	@if(count($pdfKelompoks) > 0)
			    @foreach($pdfKelompoks as $p)  	
			      	<tr>
			      		<td>{{ $p->kelompok->kelompok_data }}</td>
			      		<td>
			      			{!! Html::link(Velin::urlBackendAction('pop-nilai/'.$model->id.'?edit='.$p->id),'Edit',['class'=>'btn btn-sm btn-success','style'=>'color:white;','data-lity']) !!}
			      		
			      			{!! Html::link(Velin::urlBackendAction('delete-pdf-kelompok/'.$p->id),'Delete',['class'=>'btn btn-sm btn-danger','style'=>'color:white;','onclick'=>'return confirm("are you sure?")']) !!}
			      		</td>
			      	</tr>
			    @endforeach
			@endif
	      </tbody>
	 </table>


	 @push('scripts')
	 <script type="text/javascript">
	 	 $(function() {
	 		$.fn.dataTable.ext.errMode = 'none';
	 		$("#table_nilai").DataTable();
	 	});
	 </script>
	 @endpush

 @endif