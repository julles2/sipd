@extends('velin.layouts.pop.layout')
@section('content')
  <div style="margin-top:10px;">
    <h2>Kelompok Data</h2>
    {!! Form::model($modelData,['url'=>Velin::urlBackendAction('pop-nilai/'.$model->id.'?edit='.request()->get('edit'))]) !!}
      <div class="form-group">
        <label for="pwd">Kelompok Data:</label>
        {!! Form::select('kelompok_data_id',$kelompokDatas,null,['class'=>'form-control']) !!}
        <small style="color:red;">{{ $errors->first('kelompok_data_id') }}</small>
      </div>
      <div class="form-group">
        <label for="email">Start Year:</label>
        {!! Form::text('year',null,['class'=>'form-control','maxlength'=>4]) !!}
        <small style="color:red;">{{ $errors->first('year') }}</small>
      </div>
      <div class="form-group">
        <label for="email">End Year:</label>
        {!! Form::text('end_year',null,['class'=>'form-control','maxlength'=>4]) !!}
        <small style="color:red;">{{ $errors->first('year') }}</small>
      </div>
      <div class="form-group">
        <label for="email">Narasi:</label>
        {!! Form::textarea('narasi',null,['class'=>'ckeditor']) !!}
      </div>
      <button type="submit" class="btn btn-primary">Next</button>
    {!! Form::close() !!}
  </div>
@endsection