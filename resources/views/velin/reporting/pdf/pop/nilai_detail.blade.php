@extends('velin.layouts.pop.layout')
@section('content')
  <div style="margin-top:10px;">
    <h4>Kelompok Data : {{ $modelPdfKelompokData->kelompok->kelompok_data }}</h4>
    <hr/>
    {!! Form::model($modelPdfKelompokData) !!}
      
      <table class="table">
          <tr>  
            <th>Jenis Data</th>
            <th>-</th>
          </tr>
          @foreach($jenisDatas as $jenis)
            <tr>  
              <td>{{ $jenis->jenis_data }}</td>
              <td>
                <input {{ $checked($jenis->id) }} type = "checkbox" onclick = "triggerClick('jenis{{$jenis->id}}',this.value)" id = "jenis{{ $jenis->id }}" value = "{{ $jenis->id }}"  />
              </td>
            </tr>
          @endforeach
      </table>

      <a type="submit" class="btn btn-primary" onclick = "return alert('Data has been updated')">Update</a>
    {!! Form::close() !!}
  </div>
@endsection
@push('scripts')
<script type="text/javascript">
  
  function triggerClick(id,jenisDataId)
  {
    $checkbox = $("#"+id);

    if($checkbox.is(':checked'))
    {
      save(jenisDataId);
    }else{
      remove(jenisDataId);
    }
  }

  function save(jenisDataId)
  {
    $.ajax({
      url : '{{ Velin::urlBackendAction("ajax-save") }}',
      type : 'get',
      data : {
        pdf_kelompok_id : '{{ $modelPdfKelompokData->id }}',
        jenis_data_id : jenisDataId,
      },
      
    });
  }

  function remove(jenisDataId)
  {
    $.ajax({
      url : '{{ Velin::urlBackendAction("ajax-delete") }}',
      type : 'get',
      data : {
        pdf_kelompok_id : '{{ $modelPdfKelompokData->id }}',
        jenis_data_id : jenisDataId,
      },
      success : function(respon){
       
      },
    });
  }
</script>
@endpush