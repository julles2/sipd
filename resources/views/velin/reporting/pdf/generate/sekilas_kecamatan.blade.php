<h1>Sekilas Kecamatan</h1>

<p>&nbsp;</p>

<h3>a. Data Kecamatan</h3>

<p>&nbsp;</p>

{!! one_line($model->sekilas_data_kecamatan) !!}

<div class="page_break"></div>

<h3>b. Layanan Kecamatan</h3>

<p>&nbsp;</p>

{!! one_line($model->sekilas_layanan_kecamatan) !!}

<div class="page_break"></div>

<h3>c. Kontak Kecamatan</h3>

<p>&nbsp;</p>

{!! one_line($model->sekilas_kontak_kecamatan) !!}