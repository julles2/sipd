<h2>DAFTAR ISI</h2>
<p>&nbsp;</p>
<table width="100%" style="font-size:14px;">
	<?php
	$no = 0;
	?>
	@foreach($model->pdfKelompoks as $k)
	<?php
	$no++;
	?>		
		<tr>
			<td width="9%">BAB {{ romawi($no) }}</td>
			<td width="90%" class="name">
				<span>{{ strtoupper($k->kelompok->kelompok_data) }}</span>
			</td>
			<td width="1%">3</td>
		</tr>
		@foreach($k->pdfJenisDatas as $j)
			<tr>
				<td>&nbsp;</td>
				<td>{{ $j->jenisData->jenis_data }}</td>
				<td>&nbsp;</td>
			</tr>
		@endforeach
	@endforeach
</table>