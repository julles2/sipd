<?php $huruf = 'a'; ?>
<?php
	$role = $model->user->role_id;
	$userId = $role == 1 ? "" : $model->user->id;
	?>
@foreach($model->pdfKelompoks as $k)
	<h3>{{ $huruf }}. {{ ucwords($k->kelompok->kelompok_data) }}</h3>

	<p>&nbsp;</p>


	{!! $k->narasi !!}

	<p>&nbsp;</p>
	@foreach($k->pdfJenisDatas as $j)
			<h4>{{ $j->jenisData->jenis_data }}</h4>

			<table class="customers" style="max-width : 600px;">
					<tr>
						<th>Nama</th>
						@for($a=$k->year;$a<=$k->end_year2;$a++)
							<th>{{ $a }}</th>
						@endfor
						<th>Satuan</th>
						{{-- <th>Sumber Data</th> --}}
					</tr>
					
				<?php
				
				$no = 0;
				$parents = $repo->parents($j->jenisData->id);
				$nilaiOrFormula = function($parent,$year="",$userId="")use($model,$repo,$k,$j){
		            return $repo->nilaiOrFormula($parent,$k->year,$userId);
		        };

		        $childs = function($parents,$no,$userId)use($model,$repo,$k,$j){

		        	return $repo->childPdf($parents,$no , $k, $userId);
		    	};

				?>
				
				@foreach($parents as $parent)
				<?php $no++; ?>
				
				<tr>
					<td>{{$no}}. {{$parent->element}} {{ $parent->status }}</td>
					@for($a=$k->year;$a<=$k->end_year2;$a++)
						<td>{{ $nilaiOrFormula($parent,$a,$userId)['nilai'] }}</td>
					@endfor
					<td>{{ $parent->satuan->satuan }}</td>
					{{-- <td>{{ $nilaiOrFormula($parent)['sumber_data'] }}</td> --}}
				</tr>
				
				<?= $childs($parent,$no,$userId) ?>
				@endforeach
				
				
			</table>

			<p>&nbsp;</p>

	@endforeach

	<?php
		++$huruf;
	?>
@endforeach