@extends('velin.layouts.layout')
@section('content')
<div class="container-fluid">
    <div class="row-fluid">
        <div class="span12" id="content">
              <!-- morris stacked chart -->
            <div class="row-fluid">
                <!-- block -->
                <div class="block">
                    <div class="navbar navbar-inner block-header">
                        <div class="muted pull-left">{{ Velin::labelAction() }} - {{ $model->cover_judul }}</div>
                    </div>
                    <div class="block-content collapse in">
                        <div class="span12">

                            {!! Form::model($model,['class'=>'form-horizontal','files'=>true]) !!}
                              <fieldset>
                                <div id="tabs">
                                <ul>
                                  <li><a href="#tabs-1">Halaman Cover</a></li>
                                  <li><a href="#tabs-2">Kata Pengantar</a></li>
                                  <li><a href="#tabs-3">Peta Kabupaten</a></li>
                                  <li><a href="#tabs-4">Sekilas Kecamatan</a></li>
                                  <li><a href="#tabs-5">Daftar Isi</a></li>
                                  <li><a href="#tabs-6">Nilai Data</a></li>
                                </ul>
                                {!! Form::model($model,['class'=>'form-horizontal']) !!}
                             
                                    <div id="tabs-1">
                                      @include('velin.reporting.pdf.inc.halaman_cover')
                                    </div>
                                    <div id="tabs-2">
                                        @include('velin.reporting.pdf.inc.kata_pengantar')
                                    </div>
                                    <div id="tabs-3">
                                      @include('velin.reporting.pdf.inc.peta_kabupaten')
                                    </div>
                                    <div id="tabs-4">
                                      @include('velin.reporting.pdf.inc.sekilas')
                                    </div>
                                    <div id="tabs-5">
                                      @include('velin.reporting.pdf.inc.daftar_isi')
                                    </div>
                                    <div id="tabs-6">
                                      @include('velin.reporting.pdf.inc.nilai_index')
                                    </div>
                                {!! Form::close() !!}
                              </div>
                                <div class="form-actions">
                                  <button type="submit" class="btn btn-primary">{{ !empty($model->id) ? 'Update' : 'Create' }}</button>
                                  <?php /*  <a  target="_blank" href="{{ Velin::urlBackendAction('view-pdf/'.$model->id) }}" class="btn">View as PDF</a> */ ?>
                                  <a  id = "view_pdf" class="btn">View as PDF</a>
                                </div>
                              </fieldset>
                            {!! Form::close() !!}

                        </div>
                    </div>
                </div>
                <!-- /block -->
            </div>
        </div>
    </div>
    <hr>
    <footer>
        <p>{{ Velin::config('footer') }}</p>
    </footer>
</div>
<div id="dialog" title="Basic dialog">
  <p>
    Tahun
  </p>
  <p>
    {!! Form::select('year',$years,null,['id'=>'select_pop_year']) !!}
  </p>
  <p>
    <button type="button" id = "btn_view_pdf">View PDF</button>
  </p>
</div>

@endsection
@include('velin.common.validation')
@push('scripts')
<script type="text/javascript">
   $(document).ready(function(){
    $( "#tabs" ).tabs({
       disabled: {!! $disabled !!}
    });
    $("#sekilas_tabs").tabs();

    function anchor($form)
    {
      a_href='{{ request()->fullUrl() }}';
      a_href = a_href.replace(/(form=)[^\&]+/, '$1' + $form);


      window.location=a_href;
    }

    $(document).on('lity:close', function(event, instance) {
        anchor("");
    });


     $( "#dialog" ).dialog({
        autoOpen: false,
        show: {
          effect: "blind",
          duration: 1000
        },
        hide: {
          effect: "explode",
          duration: 1000
        }
      });

     $( "#view_pdf" ).on( "click", function() {
      $( "#dialog" ).dialog( "open" );
    });

     $("#btn_view_pdf").on('click',function(){
        

      document.location.href="{{ Velin::urlBackendAction('view-pdf/'.$model->id) }}?year=" + $("#select_pop_year").val();
     });

   });
</script>
@endpush

