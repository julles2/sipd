@extends('velin.layouts.layout')
@section('content')

@include('velin.ketersediaan_data.nilai.filter')


@if(!empty(request()->get('kelompok_data_id')) && !empty(request()->get('jenis_data_id')))
  @include('velin.ketersediaan_data.nilai.listing');
@endif

@endsection

@section('script')
  
  <script type="text/javascript">
    
    $(document).ready(function(){

        $("#cmd_update").on('click' , function(){
            window.location.reload();
        });


        $(".iframe").fancybox({
          maxWidth  : 800,
          maxHeight : 800,
          fitToView : false,
          width   : '90%',
          height    : '90%',
          autoSize  : false,
          closeClick  : false,
          openEffect  : 'none',
          closeEffect : 'none'
        });

        // kelompok_data_id event

        $("#kelompok_data_id").on('change' , function(){
            $.ajax({
              url : '{{ urlBackendAction("jenis-data") }}',
              type : 'get',
              data : {
                kelompok_data_id : $(this).val(),
              },
              success : function(data){
                $("#jenis_data_id").html("");
                $.each(data.comboJenisData , function(key ,val){
                    $("#jenis_data_id").append("<option value='"+key+"'>"+val+"</option>");
                });
              },
            });
        });

        //
        
        // event search
        $("#search").on('click' , function(){
            kelompok_data_id = $("#kelompok_data_id").val();
            jenis_data_id = $("#jenis_data_id").val();
            skpd_id = $("#skpd_id").val();
            year = $("#year").val();
            element = $("#element").val();

            if(kelompok_data_id == '' || jenis_data_id == '' || jenis_data_id == null || kelompok_data_id == null)
            {
                swal({
                  type : 'error',
                  title : 'Error Validation',
                  text : 'Kelompok Data or Jenis Data Cannot be Empty',
                });

                return false;
            }

            window.location.href='{{ urlBackendAction("index") }}' + '?kelompok_data_id=' + kelompok_data_id + '&jenis_data_id=' + jenis_data_id + '&year=' + year + '&skpd_id=' + skpd_id;
        }); 
        // 

    });

    function insertNilai(nilaiId , sumberDataId ,  id)
    {
        nilai = $("#" + nilaiId).val();

        sumber_data = $("#" + sumberDataId).val();

        jenis_data_id = $("#jenis_data_id").val();

        skpd_id = $("#skpd_id").val();

        year = $("#year").val();

        $.ajax({
          type: 'get',
          url : '{{ urlBackendAction("insert-nilai") }}',
          data : {
            id : id,
            nilai : nilai,
            sumber_data : sumber_data,
            jenis_data_id : jenis_data_id,
            skpd_id :skpd_id,
            year : year,
          },
        });
    }

  </script>

  {!! Velin::flashSuccess(Session::get('success'))  !!}
  {!! Velin::flashInfo(Session::get('info'))  !!}
@endsection