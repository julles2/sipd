<div class="container-fluid">
    <div class="row-fluid">
        <div class="span12" id="content">
              <!-- morris stacked chart -->
            <div class="row-fluid">
                <!-- block -->
                <div class="block">
                    <div class="navbar navbar-inner block-header">
                        <div class="muted pull-left">{{ Velin::labelAction() }}</div>
                    </div>
                    <div class="block-content collapse in">
                        <div class="span12">
                             <form class="form-horizontal">
                              <fieldset>
                               
                                <div class="control-group">
                                  <label class="control-label" for="focusedInput">Kelompok Data</label>
                                  <div class="controls">
                                    {!! Form::select('kelompok_data_id',['' => ''] + $kelompokDatas,request()->get('kelompok_data_id'),['class' => 'input-xxlarge','id'=>'kelompok_data_id']) !!}
                                  </div>
                                </div>
                                
                                <div class="control-group">
                                  <label class="control-label" for="focusedInput">Jenis Data</label>
                                  <div class="controls">
                                    {!! Form::select('jenis_data_id',['' => ''] + $jenisDatas,request()->get('jenis_data_id'),['class' => 'input-xxlarge','id'=>'jenis_data_id']) !!}
                                  </div>
                                </div>

                                <div class="control-group">
                                  <label class="control-label" for="focusedInput">Skpd</label>
                                  <div class="controls">
                                    {!! Form::select('skpd_id',$skpd,request()->get('skpd_id'),['class' => 'input-xxlarge','id'=>'skpd_id']) !!}
                                  </div>
                                </div>

                                <div class="control-group">
                                  <label class="control-label" for="focusedInput">Year</label>
                                  <div class="controls">
                                    {!! Form::selectRange('year', date('Y') , 2010 ,request()->get('year'),['class' => 'input-xxlarge','id'=>'year']) !!}
                                  </div>
                                </div>

                                <div class="form-actions">
                                  <button type="button" id = 'search' class="btn btn-primary">Search</button>
                                </div>

                              </fieldset>
                            </form>

                        </div>
                    </div>
                </div>
                <!-- /block -->
            </div>
        </div>
    </div>
    <hr>
    
</div>