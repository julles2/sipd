<div class="container-fluid">
    <div class="row-fluid">
        <div class="span12" id="content">
              <!-- morris stacked chart -->
            <div class="row-fluid">
                <!-- block -->
                <div class="block">
                    <div class="navbar navbar-inner block-header">
                        <div class="muted pull-left">{{ Velin::labelAction() }}</div>
                    </div>
                    <div class="block-content collapse in">
                        <div class="span12">
                        {!! Velin::buttonCreate() !!}
                            <table class="table">
                                  <thead>
                                    <tr>
                                      <th>Element</th>
                                      <th>Nilai</th>
                                      <th>Sumber Data</th>
                                      <th>Satuan</th>
                                      <th>Chart</th>
                                    </tr>
                                  </thead>
                                  <tbody>
                                  @php($no=0)
                                  @foreach($parents as $parent)
                                  @php($no++)
                                    <tr>
                                      <td>{{$no}} . {{ $parent->element }} {{ $parent->status }}</td>
                                      <td>{!! $textOrFormula($parent)['nilai'] !!}</td>
                                      <td>{!! $textOrFormula($parent)['sumber_data'] !!}</td>
                                      <td>{{ $parent->satuan->satuan }}</td>
                                      <td><a class="iframe" data-fancybox-type="iframe" href="{{ urlBackendAction('chart?id='.$parent->id.'&skpd_id='.request()->get('skpd_id')) }}" id = 'click'>Chart</a></td>
                                    </tr>
                                    <?= $childs($parent, $no) ?>
                                  @endforeach
                                  </tbody>
                                </table>
                                <div class="dataTables_paginate paging_bootstrap pagination">
                                {!! $pagination !!}
                                </div>
                        </div>
                        @if(!empty(request()->get('skpd_id')))
                          <button id = 'cmd_update' class="btn btn-primary">Update</button>
                        @endif
                    </div>
                </div>
                <!-- /block -->
            </div>
        </div>
    </div>
    <hr>
    <footer>
        <p>{{ Velin::config('footer') }}</p>
    </footer>
</div>