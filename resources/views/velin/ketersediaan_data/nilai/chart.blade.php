<!DOCTYPE html>
<html>
<head>
	<title>Chart</title>
	<script src="{{ asset(null) }}velin/vendors/jquery-1.9.1.js"></script>
	<script src="http://code.highcharts.com/highcharts.js"></script>
</head>
<body>

<form>
	<table>
		<tr>
			<td>SKPD</td>
			<td>
				  {!! Form::select('skpd_id',$skpd,request()->get('skpd_id'),['class' => 'input-xxlarge','id'=>'skpd_id']) !!}
            </td>
		</tr>
	</table>
</form>

	<div id="container" style="width:100%; height:400px;"></div>

	<script type="text/javascript">
		
		$(function () { 
			    $('#container').highcharts({
			        chart: {
			            type: 'column',
			        },
			        credits : {
			        	enabled : false,
			        },
			        title: {
			            text: 'Data {{ $model->element }}'
			        },
			        xAxis: {
			            categories: {{ $years }}
			        },
			        yAxis: {
			            title: {
			                text: 'Jumlah ({{ $model->satuan->satuan }})'
			            }
			        },
			        series: [{
			            name: '{{ $model->element }}',
			            data: {{ $datas }}
			        }]
			    });


			});

			$(document).ready(function(){
				 $("#skpd_id").on('change',function(){
			    	skpd_id = $(this).val();
			    	document.location.href= '{{ urlBackendAction("chart") }}' + '?id={{request()->get("id")}}&skpd_id=' + skpd_id;
			    });
			});
	</script>

</body>
</html>