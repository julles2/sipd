@extends('velin.layouts.layout')
@section('content')
<div class="container-fluid">
    <div class="row-fluid">
        <div class="span12" id="content">
              <!-- morris stacked chart -->
            <div class="row-fluid">
                <!-- block -->
                <div class="block">
                    <div class="navbar navbar-inner block-header">
                        <div class="muted pull-left">{{ Velin::labelAction() }}</div>
                    </div>
                    <div class="block-content collapse in">
                        <div class="span12">
                             {!! Form::model($model,['class'=>'form-horizontal','files'=>true]) !!}
                              <fieldset>
                              <div class="control-group">
                                  <label class="control-label" for="focusedInput">Judul</label>
                                  <div class="controls">
                                    {!! Form::text('judul',null,['class' => 'input-xxlarge','readonly'=>true]) !!}
                                  </div>
                                </div>

                              @for($a=0;$a<5;$a++)
                                <div class="control-group">
                                  <label class="control-label" for="focusedInput">Image</label>
                                  <div class="controls">
                                    {!! Form::file('image[]') !!}
                                  </div>
                                </div>
                              @endfor
                                <div class="form-actions">
                                  <button type="submit" class="btn btn-primary">Save</button>
                                </div>
                              </fieldset>
                            {!! Form::close() !!}

                        </div>
                    </div>
                </div>
                <!-- /block -->
            </div>
        </div>
    </div>
</div>

<div class="container-fluid">
    <div class="row-fluid">
        <div class="span12" id="content">
              <!-- morris stacked chart -->
            <div class="row-fluid">
                <!-- block -->
                <div class="block">
                    <div class="navbar navbar-inner block-header">
                        <div class="muted pull-left">All Image {{ $model->judul }}</div>
                    </div>
                    <div class="block-content collapse in">
                        <div class="span12">
                             <form class="form-horizontal">
                              <fieldset>
                               
                                <table class="table">
                                    <thead>
                                      <tr>
                                        <th width="80%">Image</th>
                                        <th width="20%">Action</th>
                                      </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($lists as $row)
                                      <tr>
                                        <td><img src="{{ asset('contents/'.$row->image) }}" width="100" height="100" /></td>
                                        <td><a onclick = 'return confirm("are you sure want to delete this image ? ")' href = '{{ urlBackendAction('delete-detail/'.$model->id.'/'.$row->id) }}' class = 'btn btn-danger btn-sm' />Delete</td>
                                      </tr>
                                    @endforeach
                                    </tbody>
                                </table>

                              </fieldset>
                            </form>

                        </div>
                    </div>
                </div>
                <!-- /block -->
            </div>
        </div>
    </div>
    <div class="pagination"> 
    {!! $lists->render() !!}
    </div>
    <hr>
    <footer>
        <p>{{ Velin::config('footer') }}</p>
    </footer>
</div>
@endsection
@section('script')
{!! Velin::flashSuccess(Session::get('success'))  !!}
@endsection
@include('velin.common.validation')