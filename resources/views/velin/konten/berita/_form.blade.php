@extends('velin.layouts.layout')
@section('content')
<div class="container-fluid">
    <div class="row-fluid">
        <div class="span12" id="content">
              <!-- morris stacked chart -->
            <div class="row-fluid">
                <!-- block -->
                <div class="block">
                    <div class="navbar navbar-inner block-header">
                        <div class="muted pull-left">{{ Velin::labelAction() }}</div>
                    </div>
                    <div class="block-content collapse in">
                        <div class="span12">
                             {!! Form::model($model,['class'=>'form-horizontal']) !!}
                              <fieldset>
                                <div class="control-group">
                                  <label class="control-label" for="focusedInput">Judul</label>
                                  <div class="controls">
                                    {!! Form::text('judul',null,['class' => 'input-xxlarge']) !!}
                                  </div>
                                </div>
                                <div class="control-group">
                                  <label class="control-label" for="focusedInput">Description</label>
                                  <div class="controls">
                                    {!! Form::textarea('description',null,['class' => 'ckeditor']) !!}
                                  </div>
                                </div>
                                <div class="control-group">
                                  <label class="control-label" for="focusedInput">Status</label>
                                  <div class="controls">
                                    {!! Form::select('status',['n'=>'un publish','y'=>'publish'],null,['class' => 'input-xxlarge']) !!}
                                  </div>
                                </div>
                                <div class="form-actions">
                                  <button type="submit" class="btn btn-primary">{{ !empty($model->id) ? 'Update' : 'Create' }}</button>
                                  <button type="reset" class="btn">Cancel</button>
                                </div>
                              </fieldset>
                            {!! Form::close() !!}

                        </div>
                    </div>
                </div>
                <!-- /block -->
            </div>
        </div>
    </div>
    <hr>
    <footer>
        <p>{{ Velin::config('footer') }}</p>
    </footer>
</div>
@endsection
@section('script')
<script type="text/javascript">
      $(document).ready(function(){
                $('.ckeditor').ckeditor();
            });
        
</script>
@endsection
@include('velin.common.validation')