@extends('velin.layouts.layout')
@section('content')
@include('velin.master_data.element.filter')
<div class="container-fluid">
    <div class="row-fluid">
        <div class="span12" id="content">
              <!-- morris stacked chart -->
            <div class="row-fluid">
                <!-- block -->
                <div class="block">
                    <div class="navbar navbar-inner block-header">
                        <div class="muted pull-left">{{ Velin::labelAction() }}</div>
                    </div>
                    <div class="block-content collapse in">
                        <div class="span12">
                        {!! Velin::buttonCreate() !!}
                            <table class="table">
                                  <thead>
                                    <tr>
                                      <th>Element</th>
                                      <th>Satuan</th>
                                      <th>Action</th>
                                    </tr>
                                  </thead>
                                  <tbody>
                                  @php($no=0)
                                  @foreach($parents as $parent)
                                  @php($no++)  
                                    <tr>
                                      <td>{{$no}} . {{ $parent->element }} {{ $parent->status }}</td>
                                      <td>{{ $parent->satuan->satuan }}</td>
                                      <td><?= Velin::buttons($parent->id) ?></td>
                                    </tr>
                                    <?= $childs($parent, $no) ?>
                                  @endforeach
                                  </tbody>
                                </table>
                                <div class="dataTables_paginate paging_bootstrap pagination">
                                {!! $pagination !!}
                                </div>
                        </div>
                    </div>
                </div>
                <!-- /block -->
            </div>
        </div>
    </div>
    <hr>
    <footer>
        <p>{{ Velin::config('footer') }}</p>
    </footer>
</div>
@endsection
@section('script')
  
  <script type="text/javascript">
    $("select").select2();
    $(document).ready(function(){

      // kelompok_data_id event

        $("#kelompok_data_id").on('change' , function(){
            $.ajax({
              url : '{{ urlBackendAction("jenis-data") }}',
              type : 'get',
              data : {
                kelompok_data_id : $(this).val(),
              },
              success : function(data){
                $("#jenis_data_id").html("");
                $.each(data.comboJenisData , function(key ,val){
                    $("#jenis_data_id").append("<option value='"+key+"'>"+val+"</option>");
                });
              },
            });
        });

        //
        
        // event search
        $("#search").on('click' , function(){
            kelompok_data_id = $("#kelompok_data_id").val();
            jenis_data_id = $("#jenis_data_id").val();
            element = $("#element").val();
            window.location.href='{{ urlBackendAction("index") }}' + '?kelompok_data_id=' + kelompok_data_id + '&jenis_data_id=' + jenis_data_id;
        }); 
        // 

    });
  </script>

  {!! Velin::flashSuccess(Session::get('success'))  !!}
  {!! Velin::flashInfo(Session::get('info'))  !!}
@endsection