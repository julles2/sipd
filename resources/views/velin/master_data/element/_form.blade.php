@extends('velin.layouts.layout')
@section('content')
<div class="container-fluid">
    <div class="row-fluid">
        <div class="span12" id="content">
              <!-- morris stacked chart -->
            <div class="row-fluid">
                <!-- block -->
                <div class="block">
                    <div class="navbar navbar-inner block-header">
                        <div class="muted pull-left">{{ Velin::labelAction() }}</div>
                    </div>
                    <div class="block-content collapse in">
                        <div class="span12">
                             {!! Form::model($model,['class'=>'form-horizontal']) !!}
                              <fieldset>
                                <div class="control-group">
                                  <label class="control-label" for="focusedInput">Kelompok Data</label>
                                  <div class="controls">
                                    {!! Form::select('kelompok_data_id',[''=>'Select Kelompok Data'] + $kelompokDatas,$kelompokDataId,['class' => 'input-xxlarge','id'=>'kelompok_data_id']) !!}
                                  </div>
                                </div>
                                <div class="control-group">
                                  <label class="control-label" for="focusedInput">Jenis Data</label>
                                  <div class="controls">
                                    {!! Form::select('jenis_data_id',$jenisDatas,null,['class' => 'input-xxlarge','id'=>'jenis_data_id']) !!}
                                  </div>
                                </div>
                                <div class="control-group">
                                  <label class="control-label" for="focusedInput">Parent</label>
                                  <div class="controls">
                                    {!! Form::select('parent_id',$elementParents,null,['class' => 'input-xxlarge','id'=>'parent_id']) !!}
                                    </div>
                                </div>
                                
                                <div class="control-group">
                                  <label class="control-label" for="focusedInput">Element</label>
                                  <div class="controls">
                                    {!! Form::text('element',null,['class' => 'input-xxlarge']) !!}
                                  </div>
                                </div>

                                <div class="control-group">
                                  <label class="control-label" for="focusedInput">Satuan</label>
                                  <div class="controls">
                                    {!! Form::select('satuan_id',$satuans,null,['class' => 'input-xxlarge','id'=>'satuan_id']) !!}
                                  </div>
                                </div>

                                <div class="control-group">
                                  <label class="control-label" for="focusedInput">Status</label>
                                  <div class="controls">
                                    {!! Form::select('status',['*'=>'*','**'=>'**','***'=>'***'],null,['class' => 'input-xxlarge','id'=>'satuan_id']) !!}
                                  </div>
                                </div>


                                <div class="form-actions">
                                  <button type="submit" class="btn btn-primary">{{ !empty($model->id) ? 'Update' : 'Create' }}</button>
                                  <button type="reset" class="btn">Cancel</button>
                                </div>
                              </fieldset>
                            {!! Form::close() !!}

                        </div>
                    </div>
                </div>
                <!-- /block -->
            </div>
        </div>
    </div>
    <hr>
    <footer>
        <p>{{ Velin::config('footer') }}</p>
    </footer>
</div>
@endsection
@include('velin.common.validation')
@section('script')
<script type="text/javascript">
  
$(document).ready(function(){
  
  // kelompok_data_id event

  $("#kelompok_data_id").on('change' , function(){
      $.ajax({
        url : '{{ urlBackendAction("jenis-data") }}',
        type : 'get',
        data : {
          kelompok_data_id : $(this).val(),
        },
        success : function(data){
          $("#jenis_data_id").html("");
          $("#jenis_data_id").append("<option value = ''>Select Jenis Data</option>");
          $.each(data.comboJenisData , function(key ,val){
              $("#jenis_data_id").append("<option value='"+key+"'>"+val+"</option>");
          });
        },
      });
  });

  //
  
  // jenis_data_id event
  $("#jenis_data_id").on('change',function(){
      $.ajax({
        url : '{{ urlBackendAction("parent") }}',
        type : 'get',
        data : {
          jenis_data_id : $(this).val(),
          id : '{{ $model->id }}',
        },
        success : function(data){
          $("#parent_id").html("");
          $("#parent_id").append("<option value = ''>Select Parent</option>");
          $.each(data.comboParent , function(key,val){
            $("#parent_id").append("<option value = '"+key+"'>"+val+"</option>");
          });
        },
      });
  });
  // 


});

$("select").select2();

</script>
@endsection