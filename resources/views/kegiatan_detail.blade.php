@extends('layouts.layout')
@section('content')
@include('layouts.sidebar_berita')

<div class='bc'><i class="fa fa-home"></i>
{!! breadcrumbs($breadcrumbs) !!}
</div>
<div class='container-fluid'>
	<div class='martop'>
		<div class='row'>
			<div class='col-md-12'>
				<form method='post' data-parsley-validate>
				<div class="panel panel-default"> 
					<div class="panel-heading">
						<h3 class="panel-title">Kegiatan Album Foto</h3>
					</div>
					<div class="panel-body">
						<div class="col-md-offset-2 col-md-8">
                            <div class="row">
                            @foreach($fotos as $foto)
                                <a data-fancybox-type="iframe" class="col-md-4" id = 'box' href = "{{ asset('contents/'.$foto->image) }}"  href = "{{ url('kegiatan/detail/'.$foto->slug) }}">
                                    <img class="img-responsive" src="{{ asset('contents/'.$foto->image) }}" style="height:200px;width:200px;" />
                                </a>
                            @endforeach
                            </div>
                            
                        </div>
					</div>
				</div>
				</form>
			</div>
			
		</div>
	</div>
</div>
@endsection
@section('script')
<script type="text/javascript">
	$(document).ready(function(){
	
		$("#box").fancybox({
          //maxWidth  : 800,
          //maxHeight : 800,
          fitToView : false,
          //width   : '100%',
          //height    : '100%',
          autoSize  : true,
          closeClick  : false,
          openEffect  : 'elastic',
          closeEffect : 'elastic'
        });

	});
</script>
@endsection