@extends('layouts.layout')
@section('content')
@include('layouts.sidebar_nilai')

<div class='bc'><i class="fa fa-home"></i> Nilai Profile</div>
<div class='container-fluid'>
	<div class='martop'>
		<div class='row'>
			<div class='col-md-12'>
				<div class="panel panel-default">
					<div class="panel-heading">
						<h3 class="panel-title">Nilai Profile</h3>
					</div>
					<div class="panel-body div-table">
						<table width='100%' cellpadding='0' cellspacing='0' class='table-cus'>
							<tr>
								<th width='10'>No.</th>
								<th>Kelompok Data</th>
							</tr>
							<?php
							$no=0;
							?>
							@foreach($kelompok as $kel)
							<?php
							$no++;
							?>
							<tr>
								<td align='center'>{{ roman($no) }}</td>
								<td>
									<b>{{ $kel->kelompok_data }}</b><br>
									Jenis Data : <br>
									<ol>
									@foreach($kel->jenisDatas as $jenis)
										<?php $strUrl = "nilai/detail?kelompok_data_id=".$kel->id."&jenis_data_id=".$jenis->id."&year=2010&skpd_id="; ?>
										<li><a href='{{ url($strUrl) }}'>{{ $jenis->jenis_data }}</a></li>
									@endforeach
									</ol>
								</td>
							</tr>
							@endforeach
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection