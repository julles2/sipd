@extends('layouts.layout')
@section('content')
@include('layouts.sidebar_berita')

<div class='bc'><i class="fa fa-home"></i>
{!! breadcrumbs($breadcrumbs) !!}
</div>
<div class='container-fluid'>
	<div class='martop'>
		<div class='row'>
			<div class='col-md-6'>
				<div class="panel panel-default"> 
					<div class="panel-body div-table">
						<table width='100%' cellpadding='0' cellspacing='0' class='table-cus'>
							<tr>
								<th width='10'>No.</th>
								<th>Tentang Bappeda</th>
							</tr>
							@php($no=0)
							@foreach($bappedas as $bappeda)
							@php($no++)
							<tr>
								<td align='center'>{{$no}}.</td>
								<td><a href = "{{ url('pemda/read/'.$bappeda->slug) }}">{{ $bappeda->judul }}</a></td>
							</tr>
							@endforeach
						</table>
					</div>
				</div>
			</div>
			
			<div class='col-md-6'>
				<div class="panel panel-default"> 
					<div class="panel-body div-table">
						<table width='100%' cellpadding='0' cellspacing='0' class='table-cus'>
							<tr>
								<th width='10'>No.</th>
								<th>Tentang Sipd</th>
							</tr>
							@php($no=0)
							@foreach($sipds as $sipd)
							@php($no++)
							<tr>
								<td align='center'>{{$no}}.</td>
								<td><a href = "{{ url('pemda/read/'.$sipd->slug) }}">{{ $sipd->judul }}</a></td>
							</tr>
							@endforeach
						</table>
					</div>
				</div>
			</div>
			

		</div>
	</div>
</div>
@endsection
@section('script')

@endsection