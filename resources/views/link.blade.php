@extends('layouts.layout')
@section('content')
@include('layouts.sidebar_berita')

<div class='bc'><i class="fa fa-home"></i>
{!! breadcrumbs($breadcrumbs) !!}
</div>
<div class='container-fluid'>
	<div class='martop'>
		<div class='row'>
			<div class='col-md-12'>
				<div class="panel panel-default"> 
					<div class="panel-body div-table">
						<table width='100%' cellpadding='0' cellspacing='0' class='table-cus'>
							<tr>
								<th width='10'>No.</th>
								<th>Link</th>
							</tr>
							@php($no=0)
							@foreach($links as $link)
							@php($no++)
							<tr>
								<td align='center'>{{$no}}.</td>
								<td><a href = "{{ $link->url }}" target="_blank">{{ $link->judul }}</a></td>
							</tr>
							@endforeach
						</table>
					</div>
				</div>
			</div>
			
		</div>
	</div>
</div>
@endsection
@section('script')

@endsection