@extends('layouts.layout')
@section('content')
@include('layouts.sidebar_berita')

<div class='bc'><i class="fa fa-home"></i>
{!! breadcrumbs($breadcrumbs) !!}
</div>
<div class='container-fluid'>
	<div class='martop'>
		<div class='row'>
			<div class='col-md-12'>
				<form method='post' data-parsley-validate>
				<div class="panel panel-default" style="height:100%;"> 
					<div class="panel-heading">
						<h3 class="panel-title">Alamat Sekretariat</h3>
					</div>
					<div class="panel-body">
						Jl. H. Somawinata, Kadu Agung, Tigaraksa, Tangerang, Banten 15720

						<div id = 'map' style="height:300px;width:100%;"></div>
					</div>
				</div>
				</form>
			</div>

			<div class='col-md-12'>
				<form method='post' class="form" data-parsley-validate>
				<div class="panel panel-default"> 
					<div class="panel-heading">
						<h3 class="panel-title">Kontak Form</h3>
					</div>

					<div class="panel-body">

							<div class="form-group">
								<b>Nama</b>
								<input type="text" class="form-control"  required>
							</div>
							<br>
							<div class="form-group">
								<b>Email</b>
								<input type="email" class="form-control" required>
							</div>					
							<br>
							<div class="form-group">
								<b>Komentar</b>
								<textarea class='form-control' required rows='3' cols='30'></textarea>
							</div> 

							<div class="form-group">
								{!! app('captcha')->display(); !!}
							</div> 
						
					</div>
					<div class="panel-footer">
						<button type='submit' class='btn btn-md btn-primary'>Submit</button>
					</div>
				</div>
				</form>
			</div>
			
		</div>
	</div>
</div>
@endsection
@section('script')
<script>


      function initMap() {
        var myLatLng = {lat: -6.267517, lng: 106.485168};

        var map = new google.maps.Map(document.getElementById('map'), {
          zoom: 11,
          center: myLatLng
        });

        var marker = new google.maps.Marker({
          position: myLatLng,
          map: map,
          title: 'Hello World!'
        });
      }
</script>
<script async defer
    src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDjb5kZTEEw3R5UfuVGiIiWkqIppFKVZEQ&callback=initMap">
    </script>
@endsection