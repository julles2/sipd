@extends('layouts.layout')
@section('content')
@include('layouts.sidebar_nilai')

<div class='bc'><i class="fa fa-home"></i> Nilai Profile</div>
<div class='container-fluid'>
	<div class='martop'>
		
		<div class="row">
			<div class='col-md-12'>
				<form method='post' data-parsley-validate>
				<div class="panel panel-default"> 
					<div class="panel-heading">
						<h3 class="panel-title">Filter</h3>
					</div>
					<div class="panel-body">
						<div class="form-group">
							<b>SKPD</b>
							{!! Form::select('skpd_id',[''=>'All'] + $selectSkpd,request()->get('skpd_id'),['class'=>'form-control','id'=>'skpd_id']) !!}
						</div>
					</div>
				</div>
				</form>
			</div>
		</div>

		 <div class="row">
			<div class='col-md-6'>
				<form method='post' data-parsley-validate>
				<div class="panel panel-default"> 
					<div class="panel-heading">
						<h3 class="panel-title">{{ $model->element.$model->status }}</h3>
					</div>
					<div class="panel-body">
						<div id = 'chart-column'></div>
					</div>
				</div>
				</form>
			</div>
			<div class='col-md-6'>
				<form method='post' data-parsley-validate>
				<div class="panel panel-default"> 
					<div class="panel-heading">
						<h3 class="panel-title">{{ $model->element.$model->status }}</h3>
					</div>
					<div class="panel-body">
						<div id = 'chart-line'></div>
					</div>
				</div>
				</form>
			</div>

			<!--div class='col-md-6'>
				<form method='post' data-parsley-validate>
				<div class="panel panel-default"> 
					<div class="panel-heading">
						<h3 class="panel-title">{{ $model->element.$model->status }}</h3>
					</div>
					<div class="panel-body">
						<div id = 'chart-pie'></div>
					</div>
				</div>
				</form>
			</div-->
		</div>
	</div>
</div>
@endsection

@section('script')

<script type="text/javascript">
		
			$(function () { 
			    $('#chart-column').highcharts({
			        chart: {
			            type: 'column',
			        },
			        credits : {
			        	enabled : false,
			        },
			        title: {
			            text: 'Grafik Data {{ $skpd.' '.$model->element.$model->status }}',
			            style : 'font-size:2px;',
			        },
			        xAxis: {
			            categories: {{ $years }}
			        },
			        yAxis: {
			            title: {
			                text: '({{ $model->satuan->satuan }})'
			            }
			        },
			        series: [{
			            name: '{{ $model->element }}',
			            data: {{ $datas }}
			        }]
			    });


			});

			$(function () { 
			    $('#chart-line').highcharts({
			        chart: {
			            type: 'line',
			        },
			        credits : {
			        	enabled : false,
			        },
			        title: {
			           text: 'Grafik Data {{ $skpd.' '.$model->element.$model->status }}',
			           style : 'font-size:2px;',
			        },
			        xAxis: {
			            categories: {{ $years }}
			        },
			        yAxis: {
			            title: {
			                text: '({{ $model->satuan->satuan }})'
			            }
			        },
			        series: [{
			            name: '{{ $model->element }}',
			            data: {{ $datas }}
			        }]
			    });


			});

			$(function () { 
			    // $('#chart-pie').highcharts({
			    //     chart: {
			    //         type: 'pie',
			    //     },
			    //     credits : {
			    //     	enabled : false,
			    //     },
			    //     title: {
			    //         text: 'Grafik Data {{ $model->element.$model->status }}'
			    //     },
			    //     series: [{
			    //         name: '{{ $model->satuan->satuan }}',
			    //         colorByPoint: true,
			    //         data: [{
			    //             name: 'Microsoft Internet Explorer',
			    //             y: 56.33
			    //         }, {
			    //             name: 'Chrome',
			    //             y: 24.03,
			    //             sliced: true,
			    //             selected: true
			    //         }, {
			    //             name: 'Firefox',
			    //             y: 10.38
			    //         }, {
			    //             name: 'Safari',
			    //             y: 4.77
			    //         }, {
			    //             name: 'Opera',
			    //             y: 0.91
			    //         }, {
			    //             name: 'Proprietary or Undetectable',
			    //             y: 0.2
			    //         }]
			    //     }]
			    // });


			});


			$(document).ready(function(){
				$("#skpd_id").on('change',function(){
					val = $(this).val();
					kelompok_data_id = '&kelompok_data_id={{ $get("kelompok_data_id") }}';
					jenis_data_id = '&jenis_data_id={{ $get("jenis_data_id") }}';
					year = '&year={{ $get("year") }}';
					skpd_id = '&skpd_id=' + val;
					element_id = 'element_id={{ $get("element_id") }}';
					strUrl = '{{ url("nilai/chart?") }}'+ element_id +kelompok_data_id + jenis_data_id + year + skpd_id;

					document.location.href=strUrl;
				});
			});	
			
	</script>

@endsection
