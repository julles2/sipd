@extends('layouts.layout')
@section('content')
@include('layouts.sidebar_dashboard')
<div class='bc'><i class="fa fa-home"></i> Dashboard</div>
<div class='container-fluid'>
	<div class='martop'>
		<div class='row'>
			<div class='col-md-12'>
				<div class="panel panel-default">
					<div class="panel-heading">
						<h3 class="panel-title">{{ $intro->judul }}</h3>
					</div>
					<div class="panel-body">
						{{ $intro->deskripsi }}
					</div>
				</div>
			</div>
		</div>
		<br>
		<div class='row'>
			<div class='col-md-6'>
				<div class="panel panel-default">
					<div class="panel-heading">
						<h3 class="panel-title">Kina *</h3>
					</div>
					<div class="panel-body thumbnail">
						<img src='{{ asset(null) }}assets/images/graf.png'>
					</div>
				</div>
			</div>
			<div class='col-md-6'>
				<div class="panel panel-default">
					<div class="panel-heading">
						<h3 class="panel-title">Sekunder</h3>
					</div>
					<div class="panel-body thumbnail">
						<img src='{{ asset(null) }}assets/images/graf.png'>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection