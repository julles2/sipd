@extends('layouts.layout')
@section('content')
@include('layouts.sidebar_berita')

<div class='bc'><i class="fa fa-home"></i>
{!! breadcrumbs($breadcrumbs) !!}
</div>
<div class='container-fluid'>
	<div class='martop'>
		<div class='row'>
			<div class='col-md-12'>
				<form method='post' data-parsley-validate>
				<div class="panel panel-default"> 
					<div class="panel-heading">
						<h3 class="panel-title">PEMDA</h3>
					</div>
					<div class="panel-body">
						<div class="col-lg-8 col-lg-offset-2 col-md-10 col-md-offset-1">
                           		<div class="post-preview">
			                            <h2 class="post-title">
			                            {{ $model->judul }}
			                            </h2>
			                        <p class="post-meta"><b>Posted on {{ $model->created_at->format("d F Y") }}</b></p>
			                        <p>{!! $model->description !!}</p>
			                    </div>
			                    <hr>
			                              
			            </div>
					</div>
					
				</div>
				</form>
			</div>
			
		</div>
	</div>
</div>
@endsection
@section('script')

@endsection