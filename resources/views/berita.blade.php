@extends('layouts.layout')
@section('content')
@include('layouts.sidebar_berita')

<div class='bc'><i class="fa fa-home"></i>
{!! breadcrumbs($breadcrumbs) !!}
</div>
<div class='container-fluid'>
	<div class='martop'>
		<div class='row'>
			<div class='col-md-12'>
				<form method='post' data-parsley-validate>
				<div class="panel panel-default"> 
					<div class="panel-heading">
						<h3 class="panel-title">Berita</h3>
					</div>
					<div class="panel-body">
						<div class="col-lg-8 col-lg-offset-2 col-md-10 col-md-offset-1">
                            
                            @foreach($lists as $row)
                            @php($url = url('berita/read/'.$row->slug))   
                                <div class="post-preview">
			                        <a href="{{ $url }}">
			                            <h2 class="post-title">
			                            {{ $row->judul }}
			                            </h2>
			                        </a>
			                        <p class="post-meta"><b>Posted on {{ $row->created_at->format("d F Y") }}</b></p>
			                        <p>{{ substr(strip_tags($row->description),0,300) }} ...</p>
			                        <p><a role="button" href="{{ $url }}" class="btn btn-primary">Selengkapnya »</a></p>
			                    </div>
			                    <hr>
			                @endforeach 

			                {!! $lists->render() !!}                  
			            </div>
					</div>
				</div>
				</form>
			</div>
			
		</div>
	</div>
</div>
@endsection
@section('script')

@endsection