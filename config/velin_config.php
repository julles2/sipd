<?php
return [
	'titleBar'		=> 'SIPD Cirebon',
	'appName'		=> 'SIPD Cirebon',
	'backendUrl'	=> 'admin',
	'footer'		=> '&copy; Bappeda kabupaten Cirebon 2016',
	'status'		=> 'development', // development or production
];